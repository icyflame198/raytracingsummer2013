#include "RayTracer.h"

#include "../Math/Ray/Ray.h"
#include "../GUI/Frame.h"

#include "../Shape/Sphere/Sphere.h"
#include "../Shape/Plane/Plane.h"
#include "../Shape/Triangle/Triangle.h"
#include "../Shape/Quad/Quad.h"

#include "wx/progdlg.h" // Progress dialog
#include <limits>

#include "../Factory/Factory.h"
#include "../MaterialDatabase/MaterialDB.h"

#include "../TinyXML/tinyxml2.h"

RayTracer::RayTracer(void) : recursionDepth(1)
{
  // Add shapes to the factory
  //
  ShapeFactory::RegisterPrototype("Sphere", new TypedPrototype<Sphere>());
  ShapeFactory::RegisterPrototype("Plane", new TypedPrototype<Plane>());
  ShapeFactory::RegisterPrototype("Triangle", new TypedPrototype<Triangle>());
  ShapeFactory::RegisterPrototype("Quad", new TypedPrototype<Quad>());

  // This will be replaced by a settings file later
  //
  LoadSettings("Settings.xml");
  cam.ClearImage(Color(0,0,0));

  //cam.LoadCameraInfo("..//..//Resources//camera.xml");
  //MaterialDatabase::LoadFromFile("..//..//Resources//materials.xml");
  //LoadScene("..//..//Resources//scene.xml");
}

RayTracer::~RayTracer(void)
{
  // Free all the memory associated with the factory and database
  //
  ShapeFactory::CleanFactory();
  MaterialDatabase::CleanDatabase();
  FreeMemory();
}

Camera& RayTracer::GetCamera(void)
{
  return cam;
}

void RayTracer::Render(wxProgressDialog* progress)
{
  // These are used for calculating every point on the image plane
  //
  float u = static_cast<float>(cam.width - 1) / 2.0f;
  float v = static_cast<float>(cam.height - 1) / 2.0f;

  // This is the center of the image plane
  //
  Point3 C(cam.imgPos);

  // Loop through every point on the image plane
  //
  for(int y = 0; y < cam.height; ++y)
  {
    for(int x = 0; x < cam.width; ++x)
    {
      // Calculate [-1, 1] value to get the current point on the image plane
      //
      float alpha = (static_cast<float>(x) / u) - 1.0f;
      float beta = (static_cast<float>(y) / v) - 1.0f;

      // Calculate our ray from the eye position to the image plane
      //
      Point3 imagePlanePoint = C + Point3(alpha * u, beta * v, 0.0f);
      Ray r(imagePlanePoint - cam.pos, imagePlanePoint, Eye_Ray);

      // The final color of the pixel
      //
      Color finalColor;

      // The closest intersection point, as well as the object index of closest intersection
      //
      float lowestT = std::numeric_limits<float>::max();
      std::vector<Object*>::iterator objectIter;

      // Loop through every object in the scene, check for collision
      //
      for(auto obj = scene.begin(); obj != scene.end(); ++obj)
      {
        float intersection;
        if((*obj)->TestIntersection(r, &intersection))
        {
          // Find the nearest collision point
          //
          lowestT = std::min(intersection, lowestT);
          objectIter = obj;
        }
      }

      // If a intersection was found
      //
      if(lowestT != std::numeric_limits<float>::max())
      {
        // If it is here then an intersection has happened
        //
        Point3 interPoint(r.FindPointOnRay(lowestT));
        Vec3 normal = (*objectIter)->shape->FindNormal(interPoint);
        Material* mat = (*objectIter)->material;

        // Run through every light in the scene and apply lighting
        //
        for(auto light = lights.begin(); light != lights.end(); ++light)
        {
          // TODO Make this work for more than just undirected point lights
          //

          // Reset lowestT for checking if a light ray is blocked by an object
          //
          lowestT = std::numeric_limits<float>::max();

          // Create a light ray from the point to the light
          //
          Ray lightRay((*light)->pos - interPoint, interPoint, Light_Ray);

          // If the dot product between the normal and light direction it means the light is facing away
          //
          if(normal.Dot(lightRay.direction) <= 0.0f)
            continue;

          // Check if there is an opaque object between the object and the light
          //
          for(auto obj = scene.begin(); obj != scene.end(); ++obj)
          {
            // Don't compare the object with it self
            //
            if(obj == objectIter)
              continue;

            float intersection;
            if((*obj)->TestIntersection(lightRay, &intersection))
            {
              // Find the nearest collision point
              //
              lowestT = std::min(intersection, lowestT);
            }
          }

          float maxT = ((*light)->pos - interPoint).Length();

          // If a collision happened continue the loop
          //
          if(lowestT != std::numeric_limits<float>::max() && lowestT < maxT)
            continue;

          // Reflection ray direction = 2 * incidentDir(Dot)normal * normal - incidentDir
          //
          Ray reflectRay((normal * 2.0f * r.direction.Dot(normal)) - r.direction, interPoint, Reflect_Ray);

          // Calculate the amount of radiance for the point, using diffuse term
          //
          float diffuse = (1.0f - mat->reflectivity) * normal.Dot(lightRay.direction) * (*light)->power;

          // Specular term is just mat->reflectivity
          //
          Vec3 rl = cam.pos - interPoint;
          rl.Normalize();
          float specular = mat->reflectivity * std::pow(rl.Dot(reflectRay.direction), 40) * (*light)->power;

          // Add the final color of the point using phong lighting
          //
          finalColor = finalColor + (mat->color * diffuse) + ((*light)->lightColor * specular);
        }
      }
      else
      {
        // If no collision occured set the color to grey, like a clear color
        //
        finalColor = Color(0.5f,0.5f,0.5f);
      }

      // Set the final pixel color
      //
      cam.SetPixel(x,y, finalColor);
    }

    // Display to the user the current progress, this will be useful for large images
    //
    int progressStatus = static_cast<int>((static_cast<float>(cam.width * y) / static_cast<float>(cam.width * cam.height)) * 100.0f);
    char buffer[5] = {0};
#pragma warning (disable:4996)
    sprintf(buffer, "%d",progressStatus);
#pragma warning (default:4996)
    progress->Update(cam.width * y, buffer);
  }
}

void RayTracer::RenderRecursive(wxProgressDialog* progress)
{
  // These are used for calculating every point on the image plane
  //
  float u = static_cast<float>(cam.width - 1) / 2.0f;
  float v = static_cast<float>(cam.height - 1) / 2.0f;

  // This is the center of the image plane
  //
  Point3 C(cam.imgPos);

  // Loop through every point on the image plane
  //
  for(int y = 0; y < cam.height; ++y)
  {
    for(int x = 0; x < cam.width; ++x)
    {
      // Calculate [-1, 1] value to get the current point on the image plane
      //
      float alpha = (static_cast<float>(x) / u) - 1.0f;
      float beta = (static_cast<float>(y) / v) - 1.0f;

      // Calculate our ray from the eye position to the image plane
      //
      Point3 imagePlanePoint = C + Point3(alpha * u, beta * v, 0.0f);
      Ray r(imagePlanePoint - cam.pos, imagePlanePoint, Eye_Ray);

      Color finalColor = CastRay(r, recursionDepth);

      // Set the final pixel color
      //
      cam.SetPixel(x,y, finalColor);
    }

    // Display to the user the current progress, this will be useful for large images
    //
    int progressStatus = static_cast<int>((static_cast<float>(cam.width * y) / static_cast<float>(cam.width * cam.height)) * 100.0f);
    char buffer[5] = {0};
#pragma warning (disable:4996)
    sprintf(buffer, "%d",progressStatus);
#pragma warning (default:4996)
    progress->Update(cam.width * y, buffer);
  }
}

Color RayTracer::CastRay(Ray& r, int depth)
{
  // The final color of the pixel
  //
  Color finalColor;

  // The closest intersection point, as well as the object index of closest intersection
  //
  float lowestT = std::numeric_limits<float>::max();
  std::vector<Object*>::iterator objectIter;

  // Loop through every object in the scene, check for collision
  //
  for(auto obj = scene.begin(); obj != scene.end(); ++obj)
  {
    float intersection;
    if((*obj)->TestIntersection(r, &intersection))
    {
      // Find the nearest collision point
      //
      if(intersection < lowestT)
      {
        lowestT = intersection;
        objectIter = obj;
      }
    }
  }

  // If a intersection was found and is less than 500000 units away, think of this as a far plane
  //
  if(lowestT != std::numeric_limits<float>::max() && lowestT < 50000.0f)
  {
    // If it is here then an intersection has happened
    //
    Point3 interPoint(r.FindPointOnRay(lowestT));
    Vec3 normal = (*objectIter)->shape->FindNormal(interPoint);
    Material* mat = (*objectIter)->material;

    const float epislon = 0.01f;
    // Reflection ray direction = incidentDir - 2 * incidentDir(Dot)normal * normal
    //
    Ray reflectRay(r.direction - (normal * 2.0f * r.direction.Dot(normal)), interPoint, Reflect_Ray);
    reflectRay.origin = reflectRay.origin + (reflectRay.direction * epislon);
    Color materialLightColor = LocalLighting(objectIter, interPoint, normal, reflectRay);

    // Add the final color of the point using phong lighting
    //
    if(depth == 0 || mat->reflectivity == 0.0f)
    {
      finalColor = finalColor + materialLightColor;
    }
    else
    {
      Color reflectColor = CastRay(reflectRay, depth - 1) * mat->reflectivity;
      finalColor = finalColor + materialLightColor + reflectColor;
    }
  }
  else
  {
    // No collision occured set the color to grey, like a clear color
    //
    //finalColor.SetColor(0.5f, 0.5f, 0.5f);
  }

  return finalColor;
}

Color RayTracer::LocalLighting(std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay)
{
  Color finalColor;

  // Run through every light in the scene and apply lighting
  //
  for(auto light = lights.begin(); light != lights.end(); ++light)
  {
    // TODO Make this work for more than just undirected point lights
    //

    // Create a light ray from the point to the light
    //
    Ray lightRay((*light)->pos - interPoint, interPoint, Light_Ray);

    // This is the radius that the light will be considered as
    //
    const float radius = 5.0f;

    // TODO Make this more modular to specifiy the number of samples per light
    //

    // 7 Points on the light, use the power as the radius
    //
    Point3 left = (*light)->pos + (Vec3(-1.0f, 0.0f, 0.0f) * radius);
    Point3 right = (*light)->pos + (Vec3(1.0f, 0.0f, 0.0f) * radius);

    Point3 up = (*light)->pos + (Vec3(0.0f, 1.0f, 0.0f) * radius);
    Point3 down = (*light)->pos + (Vec3(0.0f, -1.0f, 0.0f) * radius);

    Point3 forward = (*light)->pos + (Vec3(0.0f, 0.0f, 1.0f) * radius);
    Point3 backward = (*light)->pos + (Vec3(0.0f, 0.0f, -1.0f) * radius);

    // Cast rays to these points and get the local illumination for each
    //
    Ray l(left - interPoint, interPoint, Light_Ray);
    Ray r(right - interPoint, interPoint, Light_Ray);
    Ray u(up - interPoint, interPoint, Light_Ray);
    Ray d(down - interPoint, interPoint, Light_Ray);
    Ray f(forward - interPoint, interPoint, Light_Ray);
    Ray b(backward - interPoint, interPoint, Light_Ray);

    Color lightRayColor = ComputeColor(lightRay, light, objectIter, interPoint, normal, reflectRay);
    if(lightRay.type == Shadow_Ray)
    {
      finalColor = finalColor + ( lightRayColor * (1.0f / 7.0f) +
                               ComputeColor(l, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
                               ComputeColor(r, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
                               ComputeColor(u, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
                               ComputeColor(d, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
                               ComputeColor(f, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
                               ComputeColor(b, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f));
    }
    else
    {
      finalColor = finalColor + lightRayColor;
    }
  }

  return finalColor;
}

Color RayTracer::ComputeColor(Ray& r, std::vector<Light*>::iterator light, std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay)
{
  Material* mat = (*objectIter)->material;
  float lowestT = std::numeric_limits<float>::max();

  // If the dot product between the normal and light direction it means the light is facing away
  //
  if(normal.Dot(r.direction) <= 0.0f)
  {
    return Color();
  }

  // Reset lowestT to avoid collision errors
  //
  lowestT = std::numeric_limits<float>::max();

  // Check if there is an opaque object between the object and the light
  //
  for(auto obj = scene.begin(); obj != scene.end(); ++obj)
  {
    // Don't compare the object with it self
    //
    if(obj == objectIter)
      continue;

    float intersection;
    if((*obj)->TestIntersection(r, &intersection))
    {
      // Find the nearest collision point
      //
      lowestT = std::min(intersection, lowestT);
    }
  }

  float maxT = ((*light)->pos - interPoint).Length();

  // If a collision happened and it is between the object and the light
  //
  if(lowestT != std::numeric_limits<float>::max() && lowestT < maxT)
  {
    r.type = Shadow_Ray;
    return Color();
  }

  // Calculate the amount of radiance for the point, using diffuse term
  //
  float diffuse = (1.0f - mat->reflectivity) * normal.Dot(r.direction) * (*light)->power;

  // Specular term is just mat->reflectivity
  //
  Vec3 rl = cam.pos - interPoint;
  rl.Normalize();
  float specular = std::max(mat->reflectivity * std::pow(rl.Dot(reflectRay.direction), 4), 0.0f) * (*light)->power;

  // Calcualte local lighting using phong lighting
  //
  Color currentColor;// = (mat->color * diffuse);
  if(mat->isTex)
  {
    float u, v;
    (*objectIter)->shape->GetTexCoords(&u, &v);
    currentColor = (mat->GetTexColor(u,v) * diffuse);
  }
  else
    currentColor = (mat->color * diffuse);

  currentColor = currentColor + (currentColor * specular);
  return currentColor;
}

void RayTracer::LoadScene(const char* filename)
{
  tinyxml2::XMLDocument doc;

  // If file loaded successfully
  //
  if(doc.LoadFile(filename) == tinyxml2::XML_NO_ERROR)
  {
    for(tinyxml2::XMLElement* element = doc.FirstChildElement(); element != NULL; element = element->NextSiblingElement())
    {
      if(strcmp(element->Value(), "Object") == 0)
      {
        LoadObject(element);
      }
      else if(strcmp(element->Value(), "Light") == 0)
      {
        LoadLight(element);
      }
    }
  }
  else
  {
    // TODO proper error handling
    //
  }
}

void RayTracer::LoadObject(tinyxml2::XMLElement* element)
{
  Object* obj = new Object;
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Shape") == 0)
    {
      Shape* newShape = reinterpret_cast<Shape*>(ShapeFactory::shapes[it->Attribute("type")]->CreateType());
      newShape->Import(it);
      obj->shape = newShape;
    }
    else if(strcmp(it->Value(), "Material") == 0)
    {
      obj->material = MaterialDatabase::GetMaterial(it->Attribute("name"));
    }
  }

  scene.push_back(obj);
}

void RayTracer::LoadLight(tinyxml2::XMLElement* element)
{
  Light* l = new Light;

  int type;
  element->QueryIntAttribute("type", &type);
  l->type = static_cast<LightType>(type);

  element->QueryFloatAttribute("posX", &l->pos.x);
  element->QueryFloatAttribute("posY", &l->pos.y);
  element->QueryFloatAttribute("posZ", &l->pos.z);

  element->QueryFloatAttribute("dirX", &l->dir.x);
  element->QueryFloatAttribute("dirY", &l->dir.y);
  element->QueryFloatAttribute("dirZ", &l->dir.z);

  int red, green, blue;

  element->QueryIntAttribute("red", &red);
  element->QueryIntAttribute("green", &green);
  element->QueryIntAttribute("blue", &blue);

  l->lightColor.SetColor(red / 255.0f, green / 255.0f, blue / 255.0f);

  element->QueryFloatAttribute("power", &l->power);

  lights.push_back(l);
}

void RayTracer::Reload(void)
{
  MaterialDatabase::CleanDatabase();
  FreeMemory();

  LoadSettings("Settings.xml");
}

void RayTracer::FreeMemory(void)
{
  for(auto it = scene.begin(); it != scene.end(); ++it)
  {
    delete (*it);
  }

  scene.clear();

  for(auto it = lights.begin(); it != lights.end(); ++it)
  {
    delete (*it);
  }

  lights.clear();
}

void RayTracer::LoadSettings(const char* settingsFilename)
{
  tinyxml2::XMLDocument doc;
  
  // If file loaded successfully
  //
  if(doc.LoadFile(settingsFilename) == tinyxml2::XML_NO_ERROR)
  {
    tinyxml2::XMLElement* root = doc.FirstChildElement();

    for(tinyxml2::XMLElement* element = root; element != NULL; element = element->NextSiblingElement())
    {
      const char* name = element->Value();
      const char* fileName = element->GetText();

      // TODO Make this faster than all of the string comparisions
      //
      if(strcmp(name, "Camera") == 0)
      {
        cam.LoadCameraInfo(fileName);
      }
      else if(strcmp(name, "Materials") == 0)
      {
        MaterialDatabase::LoadFromFile(fileName);
      }
      else if(strcmp(name, "Scene") == 0)
      {
        LoadScene(fileName);
      }
      else if(strcmp(name, "RayTracer") == 0)
      {
        LoadRayTracer(fileName);
      }
    }

    // Free the memory used by the document
    //
    doc.Clear();
  }
  else
  {
    // TODO The file failed to load need some error handling here
    //
  }
}

void RayTracer::LoadRayTracer(const char* filename)
{
  tinyxml2::XMLDocument doc;
  
  // If file loaded successfully
  //
  if(doc.LoadFile(filename) == tinyxml2::XML_NO_ERROR)
  {
    tinyxml2::XMLElement* root = doc.FirstChildElement();

    for(tinyxml2::XMLElement* element = root; element != NULL; element = element->NextSiblingElement())
    {
      const char* name = element->Value();

      // TODO Make this faster than all of the string comparisions
      //
      if(strcmp(name, "RayTracer") == 0)
      {
        element->QueryIntAttribute("depth", &recursionDepth);
      }
    }

    // Free the memory used by the document
    //
    doc.Clear();
  }
  else
  {
    // TODO The file failed to load need some error handling here
    //
  }
}