#pragma once

#include "../Camera/Camera.h"
#include "../Object/Object.h"
#include "../Light/Light.h"
#include <vector>

namespace tinyxml2
{
  class XMLElement;
}

class wxProgressDialog;

class RayTracer
{

public:
  RayTracer(void);
  ~RayTracer(void);

  Camera& GetCamera(void);
  void Render(wxProgressDialog* progress); 
  void RenderRecursive(wxProgressDialog* progress);
  void Reload(void);
  void LoadScene(const char* filename);

private:
  void LoadObject(tinyxml2::XMLElement* element);
  void LoadLight(tinyxml2::XMLElement* element);
  void FreeMemory(void);
  void LoadSettings(const char* settingsFilename);
  void LoadRayTracer(const char* fileName);
  Color CastRay(Ray& r, int depth);
  Color LocalLighting(std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay);
  Color ComputeColor(Ray& r, std::vector<Light*>::iterator light,std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay);
  
  Camera cam;
  int recursionDepth;
  std::vector<Object*> scene;
  std::vector<Light*> lights;
};