#pragma once

#include <hash_map>
#include <string>

struct Material;

class MaterialDatabase
{
public:
  static void AddMaterial(std::string name, Material* mat);
  static Material* GetMaterial(std::string name);
  static void CleanDatabase(void);
  static void LoadFromFile(const char* filename);

private:
  static stdext::hash_map<std::string, Material*> materials;
};