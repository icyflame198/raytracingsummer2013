#pragma once

#include "../Parent/Shape.h"

class Torus : public Shape
{
public:
  virtual bool TestIntersection(Ray& r, float* inptersectionT);
private:
};