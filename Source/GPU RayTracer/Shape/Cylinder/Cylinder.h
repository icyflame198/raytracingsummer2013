#pragma once

#include "../Parent/Shape.h"

class Cylinder : public Shape
{
public:
  Cylinder(void);
  Cylinder(const Cylinder& rhs);
  ~Cylinder(void);
  Cylinder& operator=(const Cylinder& rhs);

  virtual bool TestIntersection(Ray& r, float* t);
  virtual void Import(tinyxml2::XMLElement* root);
  virtual Vec3 FindNormal(Point3& p);

  float radius;
private:
};