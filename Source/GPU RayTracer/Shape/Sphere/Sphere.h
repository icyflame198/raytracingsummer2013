#pragma once

#include "../Parent/Shape.h"
#include "../../Math/Ray/Ray.h"
#include "../../Math/Point/Point.h"
#include "../../Math/Vector/Vector3.h"

class Sphere : public Shape
{
public:
  Sphere(void);
  Sphere(const Sphere& rhs);
  ~Sphere(void);
  Sphere& operator=(const Sphere& rhs);

  virtual bool TestIntersection(Ray& r, float* inptersectionT);
  virtual void Import(tinyxml2::XMLElement* root);
  virtual Vec3 FindNormal(Point3& p);
  virtual void GetTexCoords(float* u, float* v);

  Point3 origin;
  float radius;

  float u;
  float v;

private:

};