#include "Plane.h"

Plane::Plane(void) : Shape("Plane"), normal(Vec3(0.0f, -1.0f, 0.0f)), pos(Point3(0.0f, 0.0f, 0.0f))
{
}

Plane::Plane(const Plane& rhs) : Shape("Plane"), normal(Vec3(0.0f, -1.0f, 0.0f)), pos(Point3(0.0f, 0.0f, 0.0f))
{
}

Plane& Plane::operator=(const Plane& rhs)
{
  if(this == &rhs)
    return *this;

  normal = rhs.normal;
  d = rhs.d;

  return *this;
}

Plane::~Plane(void)
{
}

bool Plane::TestIntersection(Ray& r, float* t)
{
  Vec3 dist = pos - r.origin;
  float nearestdist = normal.Dot(dist);
  float speed = r.direction.Dot(normal);
  *t = nearestdist / speed;

  if(*t <= 0)
    return false;

  return true;
}

void Plane::Import(tinyxml2::XMLElement* element)
{
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Pos") == 0)
    {
      it->QueryFloatAttribute("x", &pos.x);
      it->QueryFloatAttribute("y", &pos.y);
      it->QueryFloatAttribute("z", &pos.z);
    }
    else if(strcmp(it->Value(), "Normal") == 0)
    {
      it->QueryFloatAttribute("x", &normal.x);
      it->QueryFloatAttribute("y", &normal.y);
      it->QueryFloatAttribute("z", &normal.z);
      normal.Normalize();
    }
  }
}

Vec3 Plane::FindNormal(Point3& p)
{
  return normal;
}

void Plane::GetTexCoords(float* U, float* V)
{
}