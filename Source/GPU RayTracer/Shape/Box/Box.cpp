#include "Box.h"
#include "../../Math/Point/Point.h"

Box::Box(void) : Shape("Box")
{
}

Box::Box(const Box& rhs) : Shape(rhs.typeName)
{
}

Box::~Box(void)
{
}

Box& Box::operator=(const Box& rhs)
{
  if(this == &rhs)
    return *this;


  return *this;
}

bool Box::TestIntersection(Ray& r, float* t)
{
  return false;
}

void Box::Import(tinyxml2::XMLElement* element)
{
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Pos") == 0)
    {
    }
    else if(strcmp(it->Value(), "Radius") == 0)
    {
    }
  }
}

Vec3 Box::FindNormal(Point3& p)
{
  Vec3 n;

  return n;
}