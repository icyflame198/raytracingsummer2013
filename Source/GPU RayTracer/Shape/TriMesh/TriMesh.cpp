#include "TriMesh.h"
#include "../../Math/Vector/Vector3.h"
#include "../../Math/Ray/Ray.h"
#include "../../Math/Matrix/Matrix.h"

#include <amp.h>
#include <amp_math.h>
#include <fstream>

int currentTotalTris;
std::vector<Tri> tris;

Tri::Tri(void) restrict(cpu, amp)
{
}

Tri::Tri(const Tri& rhs) restrict(cpu, amp)
{
  p0 = rhs.p0;
  p1 = rhs.p1;
  p2 = rhs.p2;
  normal = rhs.normal;
}

Tri& Tri::operator=(const Tri& rhs) restrict(cpu, amp)
{
  p0 = rhs.p0;
  p1 = rhs.p1;
  p2 = rhs.p2;
  normal = rhs.normal;

  return *this;
}

Tri::~Tri(void) restrict(cpu, amp)
{
}

bool Tri::TestIntersection(RayGPU& ray, float& t) const restrict(amp)
{
  Vec3GPU u = p1 - p0;
  Vec3GPU v = p2 - p0;
  Vec3GPU w0 = ray.origin - p0;
  float a = -normal.Dot(w0);
  float b = normal.Dot(ray.direction);
  
  // If the vectors are parallel there will be no collision
  //
  if(concurrency::fast_math::fabs(b) < 0.00001f)
    return false;
  
  float r = a / b;
  
  if(r < 0.0f)
    return false;

  Point3GPU I = ray.FindPointOnRay(r);
  float uu = u.Dot(u);
  float uv = u.Dot(v);
  float vv = v.Dot(v);
  Vec3GPU w = I - p0;
  float wu = w.Dot(u);
  float wv = w.Dot(v);
  float D = uv * uv - uu * vv;

  float alpha = (uv * wv - vv * wu) / D;
  if(alpha < 0.0f || alpha > 1.0f)
    return false;

  float beta = (uv * wu - uu * wv) / D;
  if(beta < 0.0f || (alpha + beta) > 1.0f)
    return false;

  t = r;
  return true;

  //Vec3GPU a = p1 - p0;
  //Vec3GPU b = p2 - p0;
  ////Vec3GPU normal(a.CrossProduct(b));
  ////normal.Normalize();

  //t = -(r.origin - p0).Dot(normal) / r.direction.Dot(normal);

  //if(t <= 0)
  //  return false;

  //float aDotB = a.Dot(b);
  //float coeff = 1.0f / (a.SquaredLength() * b.SquaredLength() - (aDotB * aDotB));
  //Point3GPU intersection = r.FindPointOnRay(t);
  //Vec3GPU inter = (intersection - p0);
  //float interDotA = inter.Dot(a);
  //float interDotB = inter.Dot(b);
  //float alpha = coeff * (interDotA * b.SquaredLength() + -aDotB * interDotB);
  //float beta = coeff * (interDotA * -aDotB + a.SquaredLength() * interDotB);
  //float truncatedAddedValue = (alpha + beta) - 1.0f;

  //const float epsilon = 0.01f;

  //if(alpha < 0.0f || beta < 0.0f || truncatedAddedValue > epsilon)
  //  return false;

  //return true;
}

Vec3GPU Tri::FindNormal(Point3GPU& p) restrict(cpu, amp)
{
  Vec3GPU u = p1 - p0;
  Vec3GPU v = p2 - p0;
  Vec3GPU n = u.CrossProduct(v);
  n.Normalize();

  // Small hack, negate the normal, for some reason when rendering if the normal 
  //is not negated it faces the wrong direction
  //
  n = n * -1.0f;

  return n;
  //return normal;
}

Mesh::Mesh(void) restrict(cpu, amp)
{
  startIndex = 0;
  numTris = 0;
  collisionIndex = 0;
  radius = 1;

  tri = 0;

  t0.p0 = Point3GPU(1.0f, 1.0f, 0.0f);
  t0.p1 = Point3GPU(-1.0f, -1.0f, 0.0f);
  t0.p2 = Point3GPU(1.0f, -1.0f, 0.0f);

  t1.p0 = Point3GPU(-1.0f, 1.0f, 0.0f);
  t1.p1 = t0.p1;
  t1.p2 = t0.p0;
}

Mesh::~Mesh(void) restrict(cpu,amp)
{
}

Mesh::Mesh(const Mesh& rhs) restrict(cpu, amp)
{
  startIndex = rhs.startIndex;
  numTris = rhs.numTris;
  collisionIndex = rhs.collisionIndex;

  pos = rhs.pos;
  scale = rhs.scale;
  rotation = rhs.rotation;

  radius = rhs.radius;

  tri = rhs.tri;
  t0 = rhs.t0;
  t1 = rhs.t1;
}

Mesh& Mesh::operator=(const Mesh& rhs) restrict(cpu, amp)
{
  startIndex = rhs.startIndex;
  numTris = rhs.numTris;
  collisionIndex = rhs.collisionIndex;

  pos = rhs.pos;
  scale = rhs.scale;
  rotation = rhs.rotation;

  radius = rhs.radius;

  tri = rhs.tri;
  t0 = rhs.t0;
  t1 = rhs.t1;

  return *this;
}

void Mesh::Import(tinyxml2::XMLElement* root) restrict(cpu)
{
  for(auto it = root->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Pos") == 0)
    {
      it->QueryFloatAttribute("x", &pos.x);
      it->QueryFloatAttribute("y", &pos.y);
      it->QueryFloatAttribute("z", &pos.z);
    }
    else if(strcmp(it->Value(), "Radius") == 0)
    {
      it->QueryFloatAttribute("r", &radius);
    }
    else if(strcmp(it->Value(), "Tri") == 0)
    {
      tri = 1;
      radius = 0.0f;
    }
    else if(strcmp(it->Value(), "Scale") == 0)
    {
      it->QueryFloatAttribute("x", &scale.x);
      it->QueryFloatAttribute("y", &scale.y);
      it->QueryFloatAttribute("z", &scale.z);
    }
    else if(strcmp(it->Value(), "Rotation") == 0)
    {
      it->QueryFloatAttribute("x", &rotation.x);
      it->QueryFloatAttribute("y", &rotation.y);
      it->QueryFloatAttribute("z", &rotation.z);

      // The data in the files should be in degrees so convert to radians here
      //
      static const float DEG_TO_RAD = 3.141592f / 180.0f;
      rotation.x *= DEG_TO_RAD;
      rotation.y *= DEG_TO_RAD;
      rotation.z *= DEG_TO_RAD;
    }
  }

  if(tri)
  {
    Matrix transform = Matrix::ModelToWorld(pos, scale, rotation);
    t0.p0 = transform * t0.p0;
    t0.p1 = transform * t0.p1;
    t0.p2 = transform * t0.p2;

    t0.normal = t0.FindNormal(Point3GPU());

    t1.p0 = transform * t1.p0;
    t1.p1 = transform * t1.p1;
    t1.p2 = transform * t1.p2;
    t1.normal = t1.FindNormal(Point3GPU());
  }
  else
  {
    t0.p0 = Point3GPU(0.0f, 0.0f, -1000.0f);
    t0.p1 = Point3GPU(0.0f, 0.0f, -1000.0f);
    t0.p2 = Point3GPU(0.0f, 0.0f, -1000.0f);

    t1.p0 = Point3GPU(0.0f, 0.0f, -1000.0f);
    t1.p1 = Point3GPU(0.0f, 0.0f, -1000.0f);
    t1.p2 = Point3GPU(0.0f, 0.0f, -1000.0f);
  }

  //const char* filename = "";

  //for(auto it = root->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  //{
  //  if(strcmp(it->Value(), "Pos") == 0)
  //  {
  //    it->QueryFloatAttribute("x", &pos.x);
  //    it->QueryFloatAttribute("y", &pos.y);
  //    it->QueryFloatAttribute("z", &pos.z);
  //  }
  //  else if(strcmp(it->Value(), "Scale") == 0)
  //  {
  //    it->QueryFloatAttribute("x", &scale.x);
  //    it->QueryFloatAttribute("y", &scale.y);
  //    it->QueryFloatAttribute("z", &scale.z);
  //  }
  //  else if(strcmp(it->Value(), "Rotation") == 0)
  //  {
  //    it->QueryFloatAttribute("x", &rotation.x);
  //    it->QueryFloatAttribute("y", &rotation.y);
  //    it->QueryFloatAttribute("z", &rotation.z);

  //    // The data in the files should be in degrees so convert to radians here
  //    //
  //    static const float DEG_TO_RAD = 3.141592f / 180.0f;
  //    rotation.x *= DEG_TO_RAD;
  //    rotation.y *= DEG_TO_RAD;
  //    rotation.z *= DEG_TO_RAD;
  //  }
  //  else if(strcmp(it->Value(), "Filename") == 0)
  //  {
  //    filename = it->GetText();
  //  }
  //}

  //Matrix transform = Matrix::ModelToWorld(pos, scale, rotation);
  //LoadObjFile(filename, transform);
}

Vec3GPU Mesh::FindNormal(Point3GPU& p, concurrency::array<Tri, 1>& models) restrict(amp)
{
  // For Quad Support
  //
  //if(tri)
  //  return t0.normal;

  Vec3GPU norm = p - pos;
  norm.Normalize();
  return norm;
  //return models[collisionIndex].normal;
}

bool Mesh::TestIntersection(RayGPU& r, float& t, concurrency::array<Tri, 1>& models) const restrict(amp)
{
  // For Quad Support
  //
  //bool result = false;//t0.TestIntersection(r,t);// || t1.TestIntersection(r,t);
    /*if(!t0.TestIntersection(r, t))
    {
      if(t1.TestIntersection(r, t))
      {
        return true;
      }
    }
    else
    {
      return true;
    }*/

    //return false;

  Vec3GPU m = r.origin - pos;
  float b = m.Dot(r.direction); 
  float c = m.Dot(m) - (radius * radius);

  if(c > 0.0f && b > 0.0f)
  {
    return false;
  }

  float discr = (b * b) - c;

  if(discr < 0.0f)
  {
    return false;
  }

  t = -b - concurrency::fast_math::sqrtf(discr);
  if(t < 0.0f)
    t = 0.0f;

  return true;
  /*int maxIndex = startIndex + numTris;

  float lowestT = 100000.0f;

  for(int i = startIndex; i < maxIndex; ++i)
  {
    if(models[i].TestIntersection(r, t))
    {
      if(t < lowestT)
      {
        lowestT = t;
        collisionIndex = i;
      }
    }
  }

  t = lowestT;

  if(lowestT < 100000.0f)
    return true;
  
  return false;*/
}

void Mesh::GetTexCoords(float& u, float& v) restrict(cpu, amp)
{

}

void Mesh::LoadObjFile(const char* filename, Matrix& modelToWorld)
{
  const int MAX_LINE = 1024;
  // This functions reads in object files, needs to be optimized to avoid duplicate points,
  // Also it does not read in materials, fix this after engine proof
  //
  std::vector<Point3GPU> positions;
  //std::vector<D3DXVECTOR2> texCoords;
  std::vector<Vec3GPU> normals;

  int totalTris = 0;
  startIndex = tris.size();


  std::ifstream file;
  file.open(filename);

  if(file.good())
  {
    char string[MAX_LINE] = {0};
    unsigned i = 0;
    while(true)
    {
      file >> string;
      if(!strcmp(string, "#"))
      {
        // Comment do nothing
      }
      else if(!strcmp(string, "v"))
      {
        // Pos found
        float x, y, z;
        file >> x >> y >> z;
        positions.push_back(modelToWorld * Point3GPU(x, y, z));
      }
      else if(!strcmp(string, "vt"))
      {
        // Texture coord found
        float u, v;
        file >> u >> v;
        //texCoords.push_back(D3DXVECTOR2(u, v));
      }
      else if(!strcmp(string, "vn"))
      {
        // Normal found
        float x, y, z;
        file >> x >> y >> z;
        Vec3GPU normal = modelToWorld * Vec3GPU(x, y, z);
        normal.Normalize();
        normals.push_back(normal);
      }
      else if(!strcmp(string, "f"))
      {
        Tri t;
        unsigned posIndex, texCoordIndex, normalIndex;

        for( unsigned i = 0; i < 3; ++i)
        {
          file >> posIndex;
          
          switch (i)
          {
          case 0:
            t.p0 = positions.at(posIndex - 1);
            break;
            
          case 1:
            t.p1 = positions.at(posIndex - 1);
            break;

          case 2:
            t.p2 = positions.at(posIndex - 1);
            break;
          }
          //v.pos = positions.at(posIndex - 1);
          //indicies.push_back(posIndex - 1);

          if(file.peek() == '/')
          {
            file.ignore();
            if(file.peek() != '/')
            {
              // Tex coord index
              file >> texCoordIndex;
              //v.texCoord = texCoords.at(texCoordIndex - 1);
            }

            if(file.peek() == '/')
            {
              // Normal coord
              file.ignore();
              file >> normalIndex;
              t.normal = normals.at(normalIndex - 1);
              //v.normal = normals.at(normalIndex - 1);
            }
          }

          //verticies.push_back(v);
        }
        tris.push_back(t);
        ++totalTris;
      }
      else if(!strcmp(string, "mtllib"))
      {
        // For use later
      }
      else if(!strcmp(string, "usemtl"))
      {
        // For use later
      }
      else if(file.eof())
      {
        break;
      }
      
      file.ignore(1000, '\n');
      numTris = totalTris;
      SecureZeroMemory(string, MAX_LINE);
    }
  }
  else
  {
    // Log error
  }

  file.close();
}

