#pragma once

#include <amp.h>

#include "../../TinyXML/tinyxml2.h"
#include "../../Math/Point/Point.h"
#include "../../Math/Vector/Vector3.h"
struct RayGPU;
class Matrix;

struct Tri
{
  Tri(void) restrict(cpu, amp);
  Tri(const Tri& rhs) restrict(cpu, amp);
  ~Tri(void) restrict(cpu, amp);
  Tri& operator=(const Tri& rhs) restrict(cpu, amp);

  bool TestIntersection(RayGPU& r, float& inptersection) const restrict(amp);
  Vec3GPU FindNormal(Point3GPU& p) restrict(cpu, amp);
  void GetTexCoords(float& u, float& v) restrict(cpu, amp);

  Point3GPU p0;
  Point3GPU p1;
  Point3GPU p2;

  Vec3GPU normal;
};

struct Mesh
{
  Mesh(void) restrict(cpu, amp);
  ~Mesh(void) restrict(cpu, amp);
  Mesh(const Mesh& rhs) restrict(cpu,amp);
  Mesh& operator=(const Mesh& rhs) restrict(cpu, amp);

  bool TestIntersection(RayGPU& r, float& inptersectionT, concurrency::array<Tri, 1>& models) const  restrict(amp);
  void Import(tinyxml2::XMLElement* root) restrict(cpu);
  Vec3GPU FindNormal(Point3GPU& p, concurrency::array<Tri, 1>& models) restrict(amp);
  void GetTexCoords(float& u, float& v) restrict(cpu, amp);
  void LoadObjFile(const char* filename, Matrix& modelToWorld);

  float radius;

  Point3GPU pos;
  Point3GPU scale;
  Point3GPU rotation;

  int startIndex;
  int numTris;
  mutable int collisionIndex;

  int tri;

  Tri t0;
  Tri t1;
};

#include <vector>
extern int currentTotalTris;
extern std::vector<Tri> tris;