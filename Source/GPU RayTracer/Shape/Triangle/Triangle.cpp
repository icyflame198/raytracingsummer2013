#include "Triangle.h"

#include "../../Math/Matrix/Matrix.h"

Triangle::Triangle(void) : Shape("Triangle"), 
                           p0(1.0f, 1.0f, 0.0f), 
                           p1(-1.0f, -1.0f, 0.0f), 
                           p2(1.0f, -1.0f, 0.0f)
{
}

Triangle::Triangle(const Triangle& rhs) : Shape(rhs.typeName), p0(rhs.p0), p1(rhs.p1), p2(rhs.p2)
{
}

Triangle::~Triangle(void)
{
}

Triangle& Triangle::operator=(const Triangle& rhs)
{
  if(this == &rhs)
    return *this;

  p0 = rhs.p0;
  p1 = rhs.p1;
  p2 = rhs.p2;

  return *this;
}

bool Triangle::TestIntersection(Ray& r, float* t)
{
  
  Vec3 a(p1 - p0);
  Vec3 b(p2 - p0);
  Vec3 normal(a.CrossProduct(b));
  normal.Normalize();

  *t = -(r.origin - p0).Dot(normal) / r.direction.Dot(normal);

  if(*t <= 0)
    return false;

  float aDotB = a.Dot(b);
  float coeff = 1.0f / (a.SquaredLength() * b.SquaredLength() - (aDotB * aDotB));
  Point3 intersection = r.FindPointOnRay(*t);
  Vec3 inter = (intersection - p0);
  float interDotA = inter.Dot(a);
  float interDotB = inter.Dot(b);
  float alpha = coeff * (interDotA * b.SquaredLength() + -aDotB * interDotB);
  float beta = coeff * (interDotA * -aDotB + a.SquaredLength() * interDotB);

  if(alpha < 0.0f || beta < 0.0f || (alpha + beta) > 1.0f)
    return false;

  u = p0.u + alpha * (p1.u - p0.u) + beta * (p2.u - p0.u);
  v = p0.v + alpha * (p1.v - p0.v) + beta * (p2.v - p0.v);
  
  return true;
}

void Triangle::Import(tinyxml2::XMLElement* element)
{
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Pos") == 0)
    {
      it->QueryFloatAttribute("x", &pos.x);
      it->QueryFloatAttribute("y", &pos.y);
      it->QueryFloatAttribute("z", &pos.z);
    }
    else if(strcmp(it->Value(), "Scale") == 0)
    {
      it->QueryFloatAttribute("x", &scale.x);
      it->QueryFloatAttribute("y", &scale.y);
      it->QueryFloatAttribute("z", &scale.z);
    }
    else if(strcmp(it->Value(), "Rotation") == 0)
    {
      it->QueryFloatAttribute("x", &rotation.x);
      it->QueryFloatAttribute("y", &rotation.y);
      it->QueryFloatAttribute("z", &rotation.z);

      // The data in the files should be in degrees so convert to radians here
      //
      static const float DEG_TO_RAD = 3.141592f / 180.0f;
      rotation.x *= DEG_TO_RAD;
      rotation.y *= DEG_TO_RAD;
      rotation.z *= DEG_TO_RAD;
    }
  }

  Matrix transform = Matrix::ModelToWorld(pos, scale, rotation);
  p0 = transform * p0;
  p1 = transform * p1;
  p2 = transform * p2;
}

Vec3 Triangle::FindNormal(Point3& p)
{
  Vec3 u = p1 - p0;
  Vec3 v = p2 - p0;
  Vec3 n = u.CrossProduct(v);
  n.Normalize();

  // Small hack, negate the normal, for some reason when rendering if the normal 
  //is not negated it faces the wrong direction
  //
  n = n * -1.0f;

  return n;
}

void Triangle::GetTexCoords(float* U, float* V)
{
  *U = u;
  *V = v;
}