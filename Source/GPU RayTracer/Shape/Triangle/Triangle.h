#pragma once

#include "../Parent/Shape.h"

class Triangle : public Shape
{
public:
  Triangle(void);
  Triangle(const Triangle& rhs);
  ~Triangle(void);
  Triangle& operator=(const Triangle& rhs);

  virtual bool TestIntersection(Ray& r, float* inptersectionT);
  virtual void Import(tinyxml2::XMLElement* root);
  virtual Vec3 FindNormal(Point3& p);
  virtual void GetTexCoords(float* u, float* v);

  Point3 p0;
  Point3 p1;
  Point3 p2;

  Point3 pos;
  Point3 scale;
  Point3 rotation;

  float u;
  float v;
private:
};