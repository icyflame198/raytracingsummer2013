#pragma once

#include "../Parent/Shape.h"

class Cone : public Shape
{
public:
  virtual bool TestIntersection(Ray& r, float* inptersectionT);
private:
};