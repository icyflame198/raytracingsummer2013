#include "Quad.h"

#include "../../Math/Matrix/Matrix.h"

Quad::Quad(void) : Shape("Quad")
{
  // Tirangle 0 is set by default so we just need to set triangle 1
  //
  t0.p2.u = 1.0f;
  t0.p2.v = 1.0f;

  t0.p1.u = 1.0f;
  t0.p1.v = 0.0f;

  t0.p0.u = 0.0f;
  t0.p0.v = 1.0f;

  t1.p0 = Point3(-1.0f, 1.0f, 0.0f);
  t1.p1 = t0.p1;
  t1.p2 = t0.p0;

  t1.p0.u = 0.0f;
  t1.p0.v = 0.0f;
}

Quad::Quad(const Quad& rhs) : Shape(rhs.typeName)
{
}

Quad::~Quad(void)
{
}

Quad& Quad::operator=(const Quad& rhs)
{
  if(this == &rhs)
    return *this;

  t0 = rhs.t0;
  t1 = rhs.t1;

  return *this;
}

bool Quad::TestIntersection(Ray& r, float* t)
{
  t0Inter = false;
  t1Inter = false;

  if(!t0.TestIntersection(r, t))
  {
    if(t1.TestIntersection(r, t))
    {
      t1Inter = true;
      return true;
    }
  }
  else
  {
    t0Inter = true;
    return true;
  }

  return false;
}

void Quad::Import(tinyxml2::XMLElement* element)
{
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Pos") == 0)
    {
      it->QueryFloatAttribute("x", &pos.x);
      it->QueryFloatAttribute("y", &pos.y);
      it->QueryFloatAttribute("z", &pos.z);
    }
    else if(strcmp(it->Value(), "Scale") == 0)
    {
      it->QueryFloatAttribute("x", &scale.x);
      it->QueryFloatAttribute("y", &scale.y);
      it->QueryFloatAttribute("z", &scale.z);
    }
    else if(strcmp(it->Value(), "Rotation") == 0)
    {
      it->QueryFloatAttribute("x", &rotation.x);
      it->QueryFloatAttribute("y", &rotation.y);
      it->QueryFloatAttribute("z", &rotation.z);

      // The data in the files should be in degrees so convert to radians here
      //
      static const float DEG_TO_RAD = 3.141592f / 180.0f;
      rotation.x *= DEG_TO_RAD;
      rotation.y *= DEG_TO_RAD;
      rotation.z *= DEG_TO_RAD;
    }
  }

  Matrix transform = Matrix::ModelToWorld(pos, scale, rotation);
  t0.p0 = transform * t0.p0;
  t0.p1 = transform * t0.p1;
  t0.p2 = transform * t0.p2;

  t1.p0 = transform * t1.p0;
  t1.p1 = transform * t1.p1;
  t1.p2 = transform * t1.p2;

  t0.p2.u = 1.0f;
  t0.p2.v = 1.0f;

  t0.p1.u = 1.0f;
  t0.p1.v = 0.0f;

  t0.p0.u = 0.0f;
  t0.p0.v = 1.0f;

  t1.p0.u = 0.0f;
  t1.p0.v = 0.0f;

  t1.p1.u = 1.0f;
  t1.p1.v = 0.0f;

  t1.p2.u = 0.0f;
  t1.p2.v = 1.0f;

}

Vec3 Quad::FindNormal(Point3& p)
{
  return t0.FindNormal(p);
}

void Quad::GetTexCoords(float* U, float* V)
{
  if(t0Inter)
    t0.GetTexCoords(U, V);
  else if(t1Inter)
    t1.GetTexCoords(U, V);
}