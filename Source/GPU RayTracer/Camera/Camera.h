#pragma once

#include "../Math/Vector/Vector3.h"
#include "../Math/Point/Point.h"
#include "../Color/Color.h"

class Camera
{
public:
  Camera(void);
  ~Camera(void);

  void LoadCameraInfo(const char* filename);
  void SetPixel(int x, int y, Color& c);
  void GetPixel(int x, int y, Color& c);
  unsigned char* GetImagePlane(void);
  void ClearImage(Color& c);
  void CopyImage(Color* c);

  // Making the data public for now, for convience
  //
  Point3 pos;
  Point3 imgPos;
  Vec3 up;
  Vec3 right;
  Vec3 dir;

  int width;
  int height;

  unsigned char* imagePlane;
private:
};