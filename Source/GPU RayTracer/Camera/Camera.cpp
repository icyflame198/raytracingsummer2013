#include "Camera.h"

#include "../TinyXML/tinyxml2.h"
#include <string.h>

Camera::Camera(void) : pos(), 
                       up(1.0f, 0.0f, 0.0f), 
                       right(0.0f, 1.0f, 0.0f), 
                       dir(0.0f, 0.0f, 1.0f),
                       width(800),
                       height(600),
                       imagePlane(new unsigned char[width * height * 3])
{
  // Set the image to black by default
  //
  ClearImage(Color(0,0,0));
}

Camera::~Camera(void)
{
  delete[] imagePlane;
}

void Camera::LoadCameraInfo(const char* filename)
{
  tinyxml2::XMLDocument doc;
  
  // If file loaded successfully
  //
  if(doc.LoadFile(filename) == tinyxml2::XML_NO_ERROR)
  {
    tinyxml2::XMLElement* root = doc.FirstChildElement();

    for(tinyxml2::XMLElement* element = root->FirstChildElement(); element != NULL; element = element->NextSiblingElement())
    {
      const char* name = element->Value();
      const char* data = element->GetText();

      // TODO Make this faster than all of the string comparisions
      //
      if(strcmp(name, "Width") == 0)
      {
        width = atoi(data);
      }
      else if(strcmp(name, "Height") == 0)
      {
        height = atoi(data);
      }
      else if(strcmp(name, "Eye") == 0)
      {
        element->QueryFloatAttribute("x", &dir.x);
        element->QueryFloatAttribute("y", &dir.y);
        element->QueryFloatAttribute("z", &dir.z);
      }
      else if(strcmp(name, "Up") == 0)
      {
        element->QueryFloatAttribute("x", &up.x);
        element->QueryFloatAttribute("y", &up.y);
        element->QueryFloatAttribute("z", &up.z);
      }
      else if(strcmp(name, "Right") == 0)
      {
        element->QueryFloatAttribute("x", &right.x);
        element->QueryFloatAttribute("y", &right.y);
        element->QueryFloatAttribute("z", &right.z);
      }
      else if(strcmp(name, "ImgPos") == 0)
      {
        element->QueryFloatAttribute("x", &imgPos.x);
        element->QueryFloatAttribute("y", &imgPos.y);
        element->QueryFloatAttribute("z", &imgPos.z);
      }
    }

    // Delete old image plane and create new one with new sizes
    //
    delete[] imagePlane;
    imagePlane = new unsigned char[width * height * 3];

    // Calcualte where the eye location should be at
    //
    pos = Point3((dir.x * -width) + imgPos.x, (dir.y * -width) + imgPos.y, (dir.z * -width) + imgPos.z);

    // Free the memory used by the document
    //
    doc.Clear();
  }
  else
  {
    // TODO The file failed to load need some error handling here
    //
  }

}

void Camera::SetPixel(int x, int y, Color& c)
{
  if(x > width || x < 0 || y > height || y < 0)
    return;

  unsigned index = (y * width * 3) + (x * 3);
  imagePlane[index] = static_cast<unsigned char>(c.red * 255.0f);
  imagePlane[index + 1] = static_cast<unsigned char>(c.green * 255.0f);
  imagePlane[index + 2] = static_cast<unsigned char>(c.blue * 255.0f);
}

void Camera::GetPixel(int x, int y, Color& c)
{
  if(x > width || x < 0 || y > height || y < 0)
    return;

  unsigned index = (y * width) + x;
  c.red = imagePlane[index];
  c.green = imagePlane[index + 1];
  c.blue = imagePlane[index + 2];
}

unsigned char* Camera::GetImagePlane(void)
{
  return imagePlane;
}

void Camera::ClearImage(Color& c)
{
  for(int i = 0; i < width * height * 3; i += 3)
  {
    imagePlane[i] = static_cast<unsigned char>(c.red * 255.0f);
    imagePlane[i + 1] = static_cast<unsigned char>(c.green * 255.0f);
    imagePlane[i + 2] = static_cast<unsigned char>(c.blue * 255.0f);
  }
}

void Camera::CopyImage(Color* c)
{
  for(int i = 0, j = 0; i < width * height; ++i, j += 3)
  {
    imagePlane[j] = static_cast<unsigned char>(c[i].red * 255.0f);
    imagePlane[j + 1] = static_cast<unsigned char>(c[i].green * 255.0f);
    imagePlane[j + 2] = static_cast<unsigned char>(c[i].blue * 255.0f);
  }
}

