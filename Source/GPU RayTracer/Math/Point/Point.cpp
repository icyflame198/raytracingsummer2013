#include "Point.h"
#include "../Vector/Vector3.h"

Point3::Point3(void) : x(0), y(0), z(0), w(1.0f)
{
}

Point3::Point3(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_)
{
}

Point3::Point3(const Point3& p) : x(p.x), y(p.y), z(p.z), w(p.w), u(p.u), v(p.v)
{
}

float Point3::Distance(Point3& p)
{
  return (*this - p).Length();
}

float Point3::DistanceSquared(Point3& p)
{
  return (*this - p).SquaredLength();
}

Point3& Point3::operator=(const Point3& rhs)
{
  // No need to worry about self assignment due to no memory allocations
  // Also means no branching, just wasted CPU cycles
  //
  x = rhs.x;
  y = rhs.y;
  z = rhs.z;
  u = rhs.u;
  v = rhs.v;

  return *this;
}

Vec3 Point3::operator-(const Point3& rhs)
{
  return Vec3(x - rhs.x, y - rhs.y, z - rhs.z);
}

Point3 Point3::operator+(const Vec3& rhs)
{
  return Point3(x + rhs.x, y + rhs.y, z + rhs.z);
}

Point3 Point3::operator+(const Point3& rhs)
{
  return Point3(x + rhs.x, y + rhs.y, z + rhs.z);
}

// Point3 GPU code here
//

Point3GPU::Point3GPU(void) restrict(cpu, amp)
  {
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
    w = 1.0f;
}


Point3GPU::Point3GPU(float x_, float y_, float z_, float w_) restrict(cpu, amp)
{
  x = x_;
  y = y_;
  z = z_;
  w = w_;
}

Point3GPU::Point3GPU(const Point3GPU& p) restrict(cpu, amp)
{
  x = p.x;
  y = p.y;
  z = p.z;
  w = p.w;
}

float Point3GPU::Distance(Point3GPU& p) restrict(cpu, amp)
{
  return (*this - p).Length();
}

float Point3GPU::DistanceSquared(Point3GPU& p) restrict(cpu, amp)
{
  return (*this - p).SquaredLength();
}

Point3GPU& Point3GPU::operator=(const Point3GPU& rhs) restrict(cpu, amp)
{
  // No need to worry about self assignment due to no memory allocations
  // Also means no branching, just wasted CPU cycles
  //
  x = rhs.x;
  y = rhs.y;
  z = rhs.z;

  return *this;
}

Vec3GPU Point3GPU::operator-(const Point3GPU& rhs) const restrict(cpu, amp)
{
  return Vec3GPU(x - rhs.x, y - rhs.y, z - rhs.z);
}

Point3GPU Point3GPU::operator+(const Vec3GPU& rhs) restrict(cpu, amp)
{
  return Point3GPU(x + rhs.x, y + rhs.y, z + rhs.z);
}

Point3GPU Point3GPU::operator+(const Point3GPU& rhs) restrict(cpu, amp)
{
  return Point3GPU(x + rhs.x, y + rhs.y, z + rhs.z);
}