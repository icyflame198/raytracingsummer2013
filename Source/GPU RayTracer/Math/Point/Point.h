#pragma once

struct Vec3;
struct Vec3GPU;

struct Point3
{
  Point3(void);
  Point3(float x_, float y_, float z_, float w_ = 1.0f);
  Point3(const Point3& v);

  // Data
  //
  float x, y, z, w;
  float u, v;

  // Methods
  //
  float Distance(Point3& p);
  float DistanceSquared(Point3& p);

  // Assignment operator
  //
  Point3& operator=(const Point3& rhs);

  // Needed operators
  //
  Vec3 operator-(const Point3& rhs);
  Point3 operator+(const Vec3& rhs);
  Point3 operator+(const Point3& rhs);
};

struct Point3GPU
{
  Point3GPU(void) restrict(cpu, amp);
  Point3GPU(float x_, float y_, float z_, float w_ = 1.0f) restrict(cpu, amp);
  Point3GPU(const Point3GPU& v) restrict(cpu, amp);

  // Data
  //
  float x, y, z, w;
  //float u, v;

  // Methods
  //
  float Distance(Point3GPU& p) restrict(cpu, amp);
  float DistanceSquared(Point3GPU& p) restrict(cpu, amp);

  // Assignment operator
  //
  Point3GPU& operator=(const Point3GPU& rhs) restrict(cpu, amp);

  // Needed operators
  //
  Vec3GPU operator-(const Point3GPU& rhs) const restrict(cpu, amp);
  Point3GPU operator+(const Vec3GPU& rhs) restrict(cpu, amp);
  Point3GPU operator+(const Point3GPU& rhs) restrict(cpu, amp);
};