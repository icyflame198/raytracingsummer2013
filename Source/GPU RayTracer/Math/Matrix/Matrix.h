#pragma once

struct Point3;
struct Point3GPU;
struct Vec3;
struct Vec3GPU;
class Camera;

class Matrix
{
public:

  // Typical constructors
  //
  Matrix(void);
  ~Matrix(void);
  Matrix(const Matrix& rhs);
  Matrix& operator=(const Matrix& rhs);

  // Functions to generate matricies for translating objects through the 3D pipeline
  // Note this is not the full pipeline, for ray tracing there is no reason to go to projection space
  //
  static Matrix ModelToWorld(Point3& translation, Point3& scale, Point3& eulerRotation);
  static Matrix ModelToWorld(Point3GPU& translation, Point3GPU& scale, Point3GPU& eulerRotation);
  static Matrix WorldToCamera(Camera& cam);

  // Operators to make life easier
  //
  Matrix operator*(const Matrix& rhs);
  Vec3 operator*(const Vec3& rhs);
  Point3 operator*(const Point3& rhs);
  Point3GPU operator*(const Point3GPU& rhs);
  Vec3GPU operator*(const Vec3GPU& rhs);

  // Methods
  //
  void Identity(void);

  // Using a union to make accessing the matrix easier
  //
  union
  {
    struct  
    {
      float m00, m01, m02, m03,
          m10, m11, m12, m13,
          m20, m21, m22, m23,
          m30, m31, m32, m33;
    };
  
    float m[4][4];
    float mtx[16];
  };
private:
};