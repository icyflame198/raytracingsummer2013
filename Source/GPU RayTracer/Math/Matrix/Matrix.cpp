#include "Matrix.h"
#include "../Point/Point.h"
#include "../../Camera/Camera.h"
#include "../Vector/Vector3.h"

#include <cmath>

Matrix::Matrix(void)
{
  Identity();
}

Matrix::~Matrix(void)
{
}

Matrix::Matrix(const Matrix& rhs)
{
  for(int i = 0; i < 16; ++i)
  {
    mtx[i] = rhs.mtx[i];
  }
}

Matrix& Matrix::operator=(const Matrix& rhs)
{
  if(this == &rhs)
    return *this;

  for(int i = 0; i < 16; ++i)
  {
    mtx[i] = rhs.mtx[i];
  }

  return *this;
}

Matrix Matrix::ModelToWorld(Point3& translation, Point3& s, Point3& eulerRotation)
{
  // Assumed eulerRotation is converted to radians by this point
  //
  Matrix scale;
  Matrix rotX;
  Matrix rotY;
  Matrix rotZ;
  Matrix trans;

  // Set the diagonal to the scale valuess
  //
  scale.mtx[0] = s.x;
  scale.mtx[5] = s.y;
  scale.mtx[10] = s.z;

  // Generates the rotation matrix about the X axis
  //
  rotX.mtx[5] = std::cos(eulerRotation.x);
  rotX.mtx[6] = std::sin(eulerRotation.x);
  rotX.mtx[9] = -std::sin(eulerRotation.x);
  rotX.mtx[10] = std::cos(eulerRotation.x);

  // Generates the rotation matrix about the Y axis
  //
  rotY.mtx[0] = std::cos(eulerRotation.y);
  rotY.mtx[2] = -std::sin(eulerRotation.y);
  rotY.mtx[8] = std::sin(eulerRotation.y);
  rotY.mtx[10] = std::cos(eulerRotation.y);

  // Generates the rotation matrix about the Z axis
  //
  rotZ.mtx[0] = std::cos(eulerRotation.z);
  rotZ.mtx[1] = std::sin(eulerRotation.z);
  rotZ.mtx[4] = -std::sin(eulerRotation.z);
  rotZ.mtx[5] = std::cos(eulerRotation.z);

  trans.mtx[3] = translation.x;
  trans.mtx[7] = translation.y;
  trans.mtx[11] = translation.z;

  return trans * rotX * rotY * rotZ * scale;
}

Matrix Matrix::ModelToWorld(Point3GPU& translation, Point3GPU& s, Point3GPU& eulerRotation)
{
  // Assumed eulerRotation is converted to radians by this point
  //
  Matrix scale;
  Matrix rotX;
  Matrix rotY;
  Matrix rotZ;
  Matrix trans;

  // Set the diagonal to the scale valuess
  //
  scale.mtx[0] = s.x;
  scale.mtx[5] = s.y;
  scale.mtx[10] = s.z;

  // Generates the rotation matrix about the X axis
  //
  rotX.mtx[5] = std::cos(eulerRotation.x);
  rotX.mtx[6] = std::sin(eulerRotation.x);
  rotX.mtx[9] = -std::sin(eulerRotation.x);
  rotX.mtx[10] = std::cos(eulerRotation.x);

  // Generates the rotation matrix about the Y axis
  //
  rotY.mtx[0] = std::cos(eulerRotation.y);
  rotY.mtx[2] = -std::sin(eulerRotation.y);
  rotY.mtx[8] = std::sin(eulerRotation.y);
  rotY.mtx[10] = std::cos(eulerRotation.y);

  // Generates the rotation matrix about the Z axis
  //
  rotZ.mtx[0] = std::cos(eulerRotation.z);
  rotZ.mtx[1] = std::sin(eulerRotation.z);
  rotZ.mtx[4] = -std::sin(eulerRotation.z);
  rotZ.mtx[5] = std::cos(eulerRotation.z);

  trans.mtx[3] = translation.x;
  trans.mtx[7] = translation.y;
  trans.mtx[11] = translation.z;

  return trans * rotX * rotY * rotZ * scale;
}

Matrix Matrix::WorldToCamera(Camera& cam)
{
  return Matrix();
}

Matrix Matrix::operator*(const Matrix& rhs)
{
  Matrix m4;

  m4.m[0][0] =  m[0][0] * rhs.m[0][0] +  m[0][1] * rhs.m[1][0] +  m[0][2] * rhs.m[2][0] + m[0][3] * rhs.m[3][0];
  m4.m[0][1] =  m[0][0] * rhs.m[0][1] +  m[0][1] * rhs.m[1][1] +  m[0][2] * rhs.m[2][1] + m[0][3] * rhs.m[3][1];
  m4.m[0][2] =  m[0][0] * rhs.m[0][2] +  m[0][1] * rhs.m[1][2] +  m[0][2] * rhs.m[2][2] + m[0][3] * rhs.m[3][2];
  m4.m[0][3] =  m[0][0] * rhs.m[0][3] +  m[0][1] * rhs.m[1][3] +  m[0][2] * rhs.m[2][3] + m[0][3] * rhs.m[3][3];
  
  m4.m[1][0] =  m[1][0] * rhs.m[0][0] +  m[1][1] * rhs.m[1][0] +  m[1][2] * rhs.m[2][0] + m[1][3] * rhs.m[3][0];
  m4.m[1][1] =  m[1][0] * rhs.m[0][1] +  m[1][1] * rhs.m[1][1] +  m[1][2] * rhs.m[2][1] + m[1][3] * rhs.m[3][1];
  m4.m[1][2] =  m[1][0] * rhs.m[0][2] +  m[1][1] * rhs.m[1][2] +  m[1][2] * rhs.m[2][2] + m[1][3] * rhs.m[3][2];
  m4.m[1][3] =  m[1][0] * rhs.m[0][3] +  m[1][1] * rhs.m[1][3] +  m[1][2] * rhs.m[2][3] + m[1][3] * rhs.m[3][3];
  
  m4.m[2][0] =  m[2][0] * rhs.m[0][0] +  m[2][1] * rhs.m[1][0] +  m[2][2] * rhs.m[2][0] + m[2][3] * rhs.m[3][0];
  m4.m[2][1] =  m[2][0] * rhs.m[0][1] +  m[2][1] * rhs.m[1][1] +  m[2][2] * rhs.m[2][1] + m[2][3] * rhs.m[3][1];
  m4.m[2][2] =  m[2][0] * rhs.m[0][2] +  m[2][1] * rhs.m[1][2] +  m[2][2] * rhs.m[2][2] + m[2][3] * rhs.m[3][2]; 
  m4.m[2][3] =  m[2][0] * rhs.m[0][3] +  m[2][1] * rhs.m[1][3] +  m[2][2] * rhs.m[2][3] + m[2][3] * rhs.m[3][3];

  m4.m[3][0] =  m[3][0] * rhs.m[0][0] +  m[3][1] * rhs.m[1][0] +  m[3][2] * rhs.m[2][0] + m[3][3] * rhs.m[3][0];
  m4.m[3][1] =  m[3][0] * rhs.m[0][1] +  m[3][1] * rhs.m[1][1] +  m[3][2] * rhs.m[2][1] + m[3][3] * rhs.m[3][1];
  m4.m[3][2] =  m[3][0] * rhs.m[0][2] +  m[3][1] * rhs.m[1][2] +  m[3][2] * rhs.m[2][2] + m[3][3] * rhs.m[3][2]; 
  m4.m[3][3] =  m[3][0] * rhs.m[0][3] +  m[3][1] * rhs.m[1][3] +  m[3][2] * rhs.m[2][3] + m[3][3] * rhs.m[3][3];

  return m4;
}

Vec3 Matrix::operator*(const Vec3& rhs)
{
  return Vec3(rhs.x * mtx[0] + rhs.y * mtx[1] + rhs.z * mtx[2] + rhs.w * mtx[3],
              rhs.x * mtx[4] + rhs.y * mtx[5] + rhs.z * mtx[6] + rhs.w * mtx[7],
              rhs.x * mtx[8] + rhs.y * mtx[9] + rhs.z * mtx[10] + rhs.w * mtx[11],
              rhs.x * mtx[12] + rhs.y * mtx[13] + rhs.z * mtx[14] + rhs.w * mtx[15]);
}

Point3 Matrix::operator*(const Point3& rhs)
{
  return Point3(rhs.x * mtx[0] + rhs.y * mtx[1] + rhs.z * mtx[2] + rhs.w * mtx[3],
                rhs.x * mtx[4] + rhs.y * mtx[5] + rhs.z * mtx[6] + rhs.w * mtx[7],
                rhs.x * mtx[8] + rhs.y * mtx[9] + rhs.z * mtx[10] + rhs.w * mtx[11],
                rhs.x * mtx[12] + rhs.y * mtx[13] + rhs.z * mtx[14] + rhs.w * mtx[15]);
}

Point3GPU Matrix::operator*(const Point3GPU& rhs)
{
  return Point3GPU(rhs.x * mtx[0] + rhs.y * mtx[1] + rhs.z * mtx[2] + rhs.w * mtx[3],
                rhs.x * mtx[4] + rhs.y * mtx[5] + rhs.z * mtx[6] + rhs.w * mtx[7],
                rhs.x * mtx[8] + rhs.y * mtx[9] + rhs.z * mtx[10] + rhs.w * mtx[11],
                rhs.x * mtx[12] + rhs.y * mtx[13] + rhs.z * mtx[14] + rhs.w * mtx[15]);
}

Vec3GPU Matrix::operator*(const Vec3GPU& rhs)
{
  return Vec3GPU(rhs.x * mtx[0] + rhs.y * mtx[1] + rhs.z * mtx[2] + rhs.w * mtx[3],
                rhs.x * mtx[4] + rhs.y * mtx[5] + rhs.z * mtx[6] + rhs.w * mtx[7],
                rhs.x * mtx[8] + rhs.y * mtx[9] + rhs.z * mtx[10] + rhs.w * mtx[11],
                rhs.x * mtx[12] + rhs.y * mtx[13] + rhs.z * mtx[14] + rhs.w * mtx[15]);
}

void Matrix::Identity(void)
{
  for(int y = 0; y < 4; ++y)
  {
    for(int x = 0; x < 4; ++x)
    {
      // If the location is on the diagonal set it to 1 else set to 0
      //
      mtx[(y * 4) + x] = (y == x) ? 1.0f : 0.0f;
    }
  }
}

