#pragma once

#include "..\Vector\Vector3.h"
#include "..\Point\Point.h"

enum RayType
{
  Eye_Ray,
  Light_Ray,
  Reflect_Ray,
  Refract_Ray,
  Shadow_Ray
};

struct Ray
{
  Ray(void);
  ~Ray(void);
  Ray(const Ray& rhs);
  Ray& operator=(const Ray& rhs);
  Ray(Vec3& dir, Point3& pos, RayType type);

  Vec3 direction;
  Point3 origin;
  RayType type;

  Point3 FindPointOnRay(float t);
};

struct RayGPU
{
  RayGPU(void) restrict(amp);
  ~RayGPU(void) restrict(amp);
  RayGPU(const RayGPU& rhs) restrict(amp);
  RayGPU(Vec3GPU& dir, Point3GPU& pos, RayType type) restrict(amp);

  RayGPU& operator=(const RayGPU& rhs) restrict(amp);

  Point3GPU FindPointOnRay(float t) restrict(amp);

  Vec3GPU direction;
  Point3GPU origin;
  RayType type;
};