#include "Ray.h"

Ray::Ray(void) : direction(Vec3(0.0f, 0.0f, 1.0f)), origin(Point3(0.0f, 0.0f, 0.0f)), type(Eye_Ray)
{
}

Ray::Ray(const Ray& rhs) : direction(rhs.direction), origin(rhs.origin), type(rhs.type)
{
}

Ray& Ray::operator=(const Ray& rhs)
{
  if(this == &rhs)
    return *this;

  direction = rhs.direction;
  origin = rhs.origin;
  type = rhs.type;

  return *this;
}

Ray::Ray(Vec3& dir, Point3& p, RayType t) : direction(dir), origin(p), type(t)
{
  direction.Normalize();
}

Ray::~Ray(void)
{
}

Point3 Ray::FindPointOnRay(float t)
{
  return origin + direction * t;
}

// Ray GPU code is here
//

RayGPU::RayGPU(void) restrict(amp)
{
}

RayGPU::~RayGPU(void) restrict(amp)
{
}

RayGPU::RayGPU(const RayGPU& rhs) restrict(amp)
{
  direction = rhs.direction;
  origin = rhs.origin;
  type = rhs.type;
}

RayGPU::RayGPU(Vec3GPU& dir, Point3GPU& pos, RayType t) restrict(amp)
{
  direction = dir;
  direction.Normalize();
  origin = pos;
  type = t;
}

RayGPU& RayGPU::operator=(const RayGPU& rhs) restrict(amp)
{
  direction = rhs.direction;
  origin = rhs.origin;
  type = rhs.type;

  return *this;
}

Point3GPU RayGPU::FindPointOnRay(float t) restrict(amp)
{
  return origin + direction * t;
}