#pragma once

struct Point3;

struct Vec3
{
  Vec3(void);
  Vec3(float x_, float y_, float z_, float w_ = 0.0f);
  Vec3(const Vec3& v);

  // Data
  //
  float x, y, z, w;

  // Methods
  //
  float Dot(Vec3& v);
  float Dot(Point3& p);
  float Length(void);
  float SquaredLength(void);
  void Normalize(void);
  Vec3 CrossProduct(Vec3& v);

  // Assignment operator
  //
  Vec3& operator=(const Vec3& rhs);

  // Multiplication, Division, Addition, and subtraction operators
  //
  Vec3 operator*(const float rhs);
  Vec3 operator*(const Vec3& rhs);

  Vec3 operator/(const float rhs);
  Vec3 operator/(const Vec3& rhs);

  Vec3 operator+(const float rhs);
  Vec3 operator+(const Vec3& rhs);
  Point3 operator+(const Point3& rhs);

  Vec3 operator-(const float rhs);
  Vec3 operator-(const Vec3& rhs);

  Vec3& operator+=(const float rhs);
  Vec3& operator+=(const Vec3& rhs);

  Vec3& operator-=(const float rhs);
  Vec3& operator-=(const Vec3& rhs);

  Vec3& operator*=(const float rhs);
  Vec3& operator*=(const Vec3& rhs);

  Vec3& operator/=(const float rhs);
  Vec3& operator/=(const Vec3& rhs);
};

//#include <amp_math.h>
#include "../Point/Point.h"
struct Vec3GPU
{
  Vec3GPU(void) restrict(cpu, amp);
  Vec3GPU(float x_, float y_, float z_, float w_ = 0.0f) restrict(cpu, amp);
  Vec3GPU(const Vec3& v) restrict(cpu, amp);

  // Data
  //
  float x, y, z, w;

  // Methods
  //
  float Dot(Vec3GPU& v) const restrict(cpu, amp);
  float Dot(Point3GPU& p) const restrict(cpu, amp);
  float Length(void) restrict(cpu, amp);
  float SquaredLength(void) restrict(cpu, amp);
  void Normalize(void) restrict(cpu, amp);
  Vec3GPU CrossProduct(Vec3GPU& v) restrict(cpu, amp);

  // Assignment operator
  //
  Vec3GPU& operator=(const Vec3GPU& rhs) restrict(cpu, amp);

  // Multiplication, Division, Addition, and subtraction operators
  //
  Vec3GPU operator*(const float rhs) restrict(cpu, amp);
  Vec3GPU operator*(const Vec3GPU& rhs) restrict(cpu, amp);

  Vec3GPU operator/(const float rhs) restrict(cpu, amp);
  Vec3GPU operator/(const Vec3GPU& rhs) restrict(cpu, amp);

  Vec3GPU operator+(const float rhs) restrict(cpu, amp);
  Vec3GPU operator+(const Vec3GPU& rhs) restrict(cpu, amp);
  Point3GPU operator+(const Point3& rhs) restrict(cpu, amp);

  Vec3GPU operator-(const float rhs) restrict(cpu, amp);
  Vec3GPU operator-(const Vec3GPU& rhs) restrict(cpu, amp);

  Vec3GPU& operator+=(const float rhs) restrict(cpu, amp);
  Vec3GPU& operator+=(const Vec3GPU& rhs) restrict(cpu, amp);

  Vec3GPU& operator-=(const float rhs) restrict(cpu, amp);
  Vec3GPU& operator-=(const Vec3GPU& rhs) restrict(cpu, amp);

  Vec3GPU& operator*=(const float rhs) restrict(cpu, amp);
  Vec3GPU& operator*=(const Vec3GPU& rhs) restrict(cpu, amp);

  Vec3GPU& operator/=(const float rhs) restrict(cpu, amp);
  Vec3GPU& operator/=(const Vec3GPU& rhs) restrict(cpu, amp);
};