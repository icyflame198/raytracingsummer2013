#include "Vector3.h"
#include "../Point/Point.h"
#include <math.h> // sqrt

// Ctors, assignment operator
//
Vec3::Vec3(void) : x(0), y(0), z(0), w(0.0f)
{

}

Vec3::Vec3(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_)
{

}

Vec3::Vec3(const Vec3& rhs) : x(rhs.x), y(rhs.y), z(rhs.z)
{

}

Vec3& Vec3::operator=(const Vec3& rhs)
{
  // No need to worry about self assignment here since there is no allocated data
  //
  x = rhs.x;
  y = rhs.y;
  z = rhs.z;

  return *this;
}

// Methods
//
float Vec3::Dot(Vec3& v)
{
  return x * v.x + y * v.y + z * v.z;
}

float Vec3::Dot(Point3& p)
{
  return x * p.x + y * p.y + z * p.z;
}

float Vec3::Length(void)
{
  return sqrt(SquaredLength());
}

float Vec3::SquaredLength(void)
{
  return x * x + y * y + z * z;
}

void Vec3::Normalize(void)
{
  *this /= Length();
}

Vec3 Vec3::CrossProduct(Vec3& v)
{
  Vec3 crossedVec;

  crossedVec.x = y * v.z - z * v.y;
  crossedVec.y = x * v.z - z * v.x;
  crossedVec.z = x * v.y - y * v.x;

  return crossedVec;
}

// Operators
//
Vec3& Vec3::operator*=(const float rhs)
{
  x *= rhs;
  y *= rhs;
  z *= rhs;

  return *this;
}

Vec3& Vec3::operator*=(const Vec3& rhs)
{
  x *= rhs.x;
  y *= rhs.y;
  z *= rhs.z;

  return *this;
}

Vec3& Vec3::operator/=(const float rhs)
{
  x /= rhs;
  y /= rhs;
  z /= rhs;

  return *this;
}

Vec3& Vec3::operator/=(const Vec3& rhs)
{
  x /= rhs.x;
  y /= rhs.y;
  z /= rhs.z;

  return *this;
}

Vec3& Vec3::operator+=(const float rhs)
{
  x += rhs;
  y += rhs;
  z += rhs;

  return *this;
}

Vec3& Vec3::operator+=(const Vec3& rhs)
{
  x += rhs.x;
  y += rhs.y;
  z += rhs.z;

  return *this;
}

Vec3& Vec3::operator-=(const float rhs)
{
  x -= rhs;
  y -= rhs;
  z -= rhs;

  return *this;
}

Vec3& Vec3::operator-=(const Vec3& rhs)
{
  x -= rhs.x;
  y -= rhs.y;
  z -= rhs.z;

  return *this;
}

Vec3 Vec3::operator*(const float rhs)
{
  Vec3 v(*this);

  v *= rhs;

  return v;
}

Vec3 Vec3::operator*(const Vec3& rhs)
{
  Vec3 v(*this);

  v *= rhs;

  return v;
}

Vec3 Vec3::operator/(const float rhs)
{
  Vec3 v(*this);

  v /= rhs;

  return v;
}

Vec3 Vec3::operator/(const Vec3& rhs)
{
  Vec3 v(*this);

  v /= rhs;

  return v;
}

Vec3 Vec3::operator+(const float rhs)
{
  Vec3 v(*this);

  v += rhs;

  return v;
}

Vec3 Vec3::operator+(const Vec3& rhs)
{
  Vec3 v(*this);

  v += rhs;

  return v;
}

Point3 Vec3::operator+(const Point3& rhs)
{
  return Point3(x + rhs.x, y + rhs.y, z + rhs.z);
}

Vec3 Vec3::operator-(const float rhs)
{
  Vec3 v(*this);

  v -= rhs;

  return v;
}

Vec3 Vec3::operator-(const Vec3& rhs)
{
  Vec3 v(*this);

  v -= rhs;

  return v;
}

/////////////////////////////////////////////////////////
// Vector3 GPU code here
/////////////////////////////////////////////////////////
#include <amp_math.h>
Vec3GPU::Vec3GPU(void) restrict(cpu, amp)
{
  x = 0.0f;
  y = 0.0f;
  z = 0.0f;
  w = 0.0f;
}

Vec3GPU::Vec3GPU(float x_, float y_, float z_, float w_) restrict(cpu, amp)
{
  x = x_;
  y = y_;
  z = z_;
  w = w_;
}
Vec3GPU::Vec3GPU(const Vec3& v) restrict(cpu, amp)
{
  x = v.x;
  y = v.y;
  z = v.z;
  w = v.w;
}


// Methods
//
float Vec3GPU::Dot(Vec3GPU& v) const restrict(cpu, amp)
{
  return x * v.x + y * v.y + z * v.z;
}
float Vec3GPU::Dot(Point3GPU& p) const restrict(cpu, amp)
{
  return x * p.x + y * p.y + z * p.z;
}
float Vec3GPU::Length(void) restrict(cpu, amp)
{
  return concurrency::fast_math::sqrtf(SquaredLength());
}
float Vec3GPU::SquaredLength(void) restrict(cpu, amp)
{
  return x * x + y * y + z * z;
}
void Vec3GPU::Normalize(void) restrict(cpu, amp)
{
  *this /= Length();
}
Vec3GPU Vec3GPU::CrossProduct(Vec3GPU& v) restrict(cpu, amp)
{
  return Vec3GPU( y * v.z - z * v.y, x * v.z - z * v.x, x * v.y - y * v.x);
}

// Assignment operator
//
Vec3GPU& Vec3GPU::operator=(const Vec3GPU& rhs) restrict(cpu, amp)
{
  x = rhs.x;
  y = rhs.y;
  z = rhs.z;

  return *this;
}

// Multiplication, Division, Addition, and subtraction operators
//
Vec3GPU Vec3GPU::operator*(const float rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v *= rhs;

  return v;
}
Vec3GPU Vec3GPU::operator*(const Vec3GPU& rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v *= rhs;

  return v;
}

Vec3GPU Vec3GPU::operator/(const float rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v /= rhs;

  return v;
}
Vec3GPU Vec3GPU::operator/(const Vec3GPU& rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v /= rhs;

  return v;
}

Vec3GPU Vec3GPU::operator+(const float rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v += rhs;

  return v;
}
Vec3GPU Vec3GPU::operator+(const Vec3GPU& rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v += rhs;

  return v;
}
Point3GPU Vec3GPU::operator+(const Point3& rhs) restrict(cpu, amp)
{
  return Point3GPU(x + rhs.x, y + rhs.y, z + rhs.z);
}

Vec3GPU Vec3GPU::operator-(const float rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v -= rhs;

  return v;
}
Vec3GPU Vec3GPU::operator-(const Vec3GPU& rhs) restrict(cpu, amp)
{
  Vec3GPU v(*this);

  v -= rhs;

  return v;
}

Vec3GPU& Vec3GPU::operator+=(const float rhs) restrict(cpu, amp)
{
  x += rhs;
  y += rhs;
  z += rhs;

  return *this;
}
Vec3GPU& Vec3GPU::operator+=(const Vec3GPU& rhs) restrict(cpu, amp)
{
  x += rhs.x;
  y += rhs.y;
  z += rhs.z;

  return *this;
}

Vec3GPU& Vec3GPU::operator-=(const float rhs) restrict(cpu, amp)
{
  x -= rhs;
  y -= rhs;
  z -= rhs;

  return *this;
}
Vec3GPU& Vec3GPU::operator-=(const Vec3GPU& rhs) restrict(cpu, amp)
{
  x -= rhs.x;
  y -= rhs.y;
  z -= rhs.z;

  return *this;
}

Vec3GPU& Vec3GPU::operator*=(const float rhs) restrict(cpu, amp)
{
  x *= rhs;
  y *= rhs;
  z *= rhs;

  return *this;
}
Vec3GPU& Vec3GPU::operator*=(const Vec3GPU& rhs) restrict(cpu, amp)
{
  x *= rhs.x;
  y *= rhs.y;
  z *= rhs.z;

  return *this;
}

Vec3GPU& Vec3GPU::operator/=(const float rhs) restrict(cpu, amp)
{
  x /= rhs;
  y /= rhs;
  z /= rhs;

  return *this;
}
Vec3GPU& Vec3GPU::operator/=(const Vec3GPU& rhs) restrict(cpu, amp)
{
  x /= rhs.x;
  y /= rhs.y;
  z /= rhs.z;

  return *this;
}







