#pragma once

struct Color
{
  Color(void) restrict(cpu, amp);
  Color(float r, float g, float b) restrict(cpu, amp);
  Color(const Color& c) restrict(cpu, amp);
  Color& operator=(const Color& c) restrict(cpu, amp);

  void SetColor(float r, float g, float b) restrict(cpu, amp);

  Color operator+(const Color& rhs) restrict(cpu, amp);
  Color operator*(const float rhs) restrict(cpu, amp);

  float red;
  float green;
  float blue;
};