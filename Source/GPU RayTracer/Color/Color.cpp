#include "Color.h"
#include <algorithm>

Color::Color(void) restrict(cpu, amp)
{
  red = 0.0f;
  green = 0.0f;
  blue = 0.0f;
}

Color::Color(float r, float g, float b) restrict(cpu, amp) 
{
  red = r < 1.0f ? r : 1.0f;
  green = g < 1.0f ? g : 1.0f;
  blue = b < 1.0f ? b : 1.0f;
}

Color::Color(const Color& c) restrict(cpu, amp)
{
  red = c.red;
  green = c.green;
  blue = c.blue;
}

Color& Color::operator=(const Color& c) restrict(cpu, amp)
{
  red = c.red;
  green = c.green;
  blue = c.blue;

  return *this;
}

Color Color::operator+(const Color& rhs) restrict(cpu, amp)
{
  return Color(red + rhs.red, green + rhs.green, blue + rhs.blue);
}

Color Color::operator*(const float rhs) restrict(cpu, amp)
{
  return Color(red * rhs, green * rhs, blue * rhs);
}

void Color::SetColor(float r, float g, float b) restrict(cpu, amp)
{
  // Using floats for more precision
  //
   red = r < 1.0f ? r : 1.0f;
  green = g < 1.0f ? g : 1.0f;
  blue = b < 1.0f ? b : 1.0f;
}