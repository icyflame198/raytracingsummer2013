#include "Light.h"

LightGPU::LightGPU(void) restrict(cpu, amp)
{

}

LightGPU::~LightGPU(void) restrict(cpu, amp)
{

}

LightGPU::LightGPU(const LightGPU& rhs) restrict(cpu, amp)
{
  pos = rhs.pos;
  power = rhs.power;
}

LightGPU& LightGPU::operator=(const LightGPU& rhs) restrict(cpu, amp)
{
  pos = rhs.pos;
  power = rhs.power;

  return *this;
}

void LightGPU::Update(void) restrict(cpu)
{
  //pos.z -= 1.0f;
  //pos.y += 0.5f;
}