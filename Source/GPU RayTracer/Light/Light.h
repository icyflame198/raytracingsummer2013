#pragma once

#include "../Math/Vector/Vector3.h"
#include "../Math/Point/Point.h"
#include "../Color/Color.h"

enum LightType
{
  Point_Light,
  Spot_Light,
  Directional_Light,
  Directional_Point_Light
};

struct Light
{
  LightType type;

  Vec3 dir;
  Point3 pos;
  float power;
  Color lightColor;
};

struct LightGPU
{
  LightGPU(void) restrict(cpu, amp);
  ~LightGPU(void) restrict(cpu, amp);
  LightGPU(const LightGPU& l) restrict(cpu, amp);
  LightGPU& operator=(const LightGPU& rhs) restrict(cpu, amp);

  void Update(void) restrict(cpu);

  Point3GPU pos;
  float power;
};