#include "Material.h"

Material::Material(void)
{
}

Material::Material(Color& c, float r, bool isTexture) : color(c), reflectivity(r), isTex(isTexture), texture(0)
{
}

Material::~Material(void)
{
  if(texture)
    delete[] texture;
}

Color Material::GetTexColor(float u, float v)
{
  int texU = static_cast<int>(u * 9.0f);
  int texV = static_cast<int>(v * 9.0f);

  return texture[(texV * 10) + texU];
}

void Material::LoadTexture(const char* filename)
{
  if(texture)
    delete[] texture;

  // Generate checkerboard pattern
  //
  texture = new Color[10 * 10];
  
  bool color = false;

  for(int i = 0; i < 100; ++i)
  {
    if(color)
      texture[i].SetColor(1.0f, 1.0f, 1.0f);
    else
      texture[i].SetColor(0.0f, 0.0f, 0.0f);

    color = !color;
  }
}

// Material GPU code here
//
MaterialGPU::MaterialGPU(void) restrict(cpu, amp)
{
}

MaterialGPU::MaterialGPU(Color& c, float r) restrict(cpu, amp)
{
  color.red = c.red;
  color.green = c.green;
  color.blue = c.blue;

  reflectivity = r;
}

MaterialGPU::~MaterialGPU(void) restrict(cpu, amp)
{
}

MaterialGPU& MaterialGPU::operator=(const MaterialGPU& rhs) restrict(cpu, amp)
{
  color = rhs.color;

  reflectivity = rhs.reflectivity;

  return *this;
}

MaterialGPU& MaterialGPU::operator=(const Material& rhs)
{
  color = rhs.color;
  reflectivity = rhs.reflectivity;

  return *this;
}