#pragma once

#include "../Color/Color.h"

struct Material
{
  Material(void);
  Material(Color& c, float r, bool texture);
  ~Material(void);

  Color color;
  float reflectivity;
  bool isTex;
  Color* texture;

  Color GetTexColor(float u, float v);
  void LoadTexture(const char* filename);
};

struct MaterialGPU
{
  MaterialGPU(void) restrict(cpu, amp);
  MaterialGPU(Color& c, float r) restrict(cpu, amp);
  ~MaterialGPU(void) restrict(cpu, amp);
  MaterialGPU& operator=(const MaterialGPU& rhs) restrict(cpu, amp); 
  MaterialGPU& operator=(const Material& rhs);

  Color color;
  float reflectivity;
};