#include "../RayTracer/RayTracer.h"

#include <array>
#include <assert.h>
#include <amp_graphics.h>
#include <fstream>
#include <mmsystem.h>

/* 
   This is not my code but was taken from C++ AMP Tech Demo Image Effects 
   Found here: http://blogs.msdn.com/b/nativeconcurrency/archive/2012/07/13/image-effects-sample-in-c-amp.aspx
*/
std::unique_ptr<char[]> ReadShader(const char * file, size_t & length)
{
    std::ifstream ifstream(file, std::ios::binary);
    if (!ifstream.good()) assert(false);
    ifstream.seekg(0, std::ios::end);
    length = static_cast<size_t>(ifstream.tellg());
    ifstream.seekg(0, std::ios::beg);
    std::unique_ptr<char[]> p(new char[length]);
    ifstream.read(p.get(), length);
    ifstream.close();
    return p;
}

struct vertex
{
  concurrency::graphics::direct3d::float3 Pos; 
  concurrency::graphics::direct3d::float2 Tex;
};

// Just this file needs this bool
//
bool open;

// Need this 
//
HWND windowHandle;
CComPtr<ID3D11VertexShader> vertexShader;
CComPtr<ID3D11PixelShader> pixelShader;
CComPtr<ID3D11InputLayout> vertexLayout;
ID3D11SamplerState* samplerState;
ID3D11Buffer* vertexBuffer;
CComPtr<ID3D11Device> device;
CComPtr<ID3D11DeviceContext> context;
CComPtr<IDXGISwapChain> swapChain;
ID3D11ShaderResourceView* outputView;

void KeyDown(WPARAM key)
{
  switch(key)
  {
  case VK_ESCAPE:
    // Exit here
    open = false;
    break;

  case 'A':
    left = true;
    break;

  case 'D':
    right = true;
    break;

  case VK_SPACE:
    fire = true;
    break;

  }
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  switch(message)
  {
  case WM_KEYDOWN:
    KeyDown(wParam);
    break;
  }

  // If I don't handle the input let the window handle it
  //
  return DefWindowProc(hWnd, message, wParam, lParam);
}

void InitWindow(HINSTANCE hInst)
{
  WNDCLASSEX wcex;
  SecureZeroMemory(&wcex, sizeof(WNDCLASSEX));

  wcex.cbSize = sizeof(WNDCLASSEX);
  wcex.style = CS_HREDRAW | CS_VREDRAW | CS_NOCLOSE;
  wcex.lpfnWndProc = (WNDPROC)WndProc;
  wcex.cbWndExtra = 0;
  wcex.cbClsExtra = 0;
  wcex.hInstance = hInst;
  wcex.hIcon = 0;
  wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
  wcex.lpszClassName = L"WindowClass";
  wcex.lpszMenuName = NULL;
  wcex.hIconSm = NULL;

  RegisterClassEx(&wcex);

  // WS_POPUP|WS_VISIBLE|WS_SYSMENU For no border window

  windowHandle = CreateWindow(L"WindowClass", L"Real Time Ray Tracer", WS_VISIBLE, 0, 0, g_rayTracerCam.width, g_rayTracerCam.height, NULL, NULL, hInst, NULL);

  ShowWindow(windowHandle, SW_SHOW);
  UpdateWindow(windowHandle);
}

void DestroyWindow(HINSTANCE hInst)
{
  UnregisterClass(L"WindowClass", hInst);
}

void InitDirectX(void)
{
  HRESULT hr = S_OK;
  UINT deviceFlags = 0;

#ifdef _DEBUG
  deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

  std::array<D3D_FEATURE_LEVEL, 1> featureLevels = { D3D_FEATURE_LEVEL_11_0 };
  D3D_FEATURE_LEVEL featureLevel;

  DXGI_SWAP_CHAIN_DESC swapChainDesc;
  SecureZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

  swapChainDesc.BufferDesc.Width = g_rayTracerCam.width;
  swapChainDesc.BufferDesc.Height = g_rayTracerCam.height;
  swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
  swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
  swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
  swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

  swapChainDesc.SampleDesc.Count = 1;
  swapChainDesc.SampleDesc.Quality = 0;

  swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
  swapChainDesc.BufferCount = 2;
  swapChainDesc.OutputWindow = windowHandle;
  swapChainDesc.Windowed = true;
  swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
  swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

  hr = D3D11CreateDeviceAndSwapChain(nullptr,
    D3D_DRIVER_TYPE_HARDWARE,
    nullptr,
    deviceFlags,
    featureLevels.data(),
    UINT(featureLevels.size()),
    D3D11_SDK_VERSION,
    &swapChainDesc,
    &swapChain,
    &device,
    &featureLevel,
    &context);

  av = concurrency::direct3d::create_accelerator_view(device);

  size_t shaderLength;
  auto shader = ReadShader("vs.cso", shaderLength);
  hr = device->CreateVertexShader(shader.get(), shaderLength, nullptr, &vertexShader);

  assert(SUCCEEDED(hr));

  D3D11_INPUT_ELEMENT_DESC layout[] =
  {
    { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
  };
  UINT numElements = ARRAYSIZE( layout );
  
  hr = device->CreateInputLayout(layout, numElements, shader.get(), shaderLength, &vertexLayout);

  assert(SUCCEEDED(hr));

  context->IASetInputLayout(vertexLayout);

  shader = ReadShader("ps.cso", shaderLength);
  hr = device->CreatePixelShader(shader.get(), shaderLength, nullptr, &pixelShader);

  assert(SUCCEEDED(hr));

  D3D11_SAMPLER_DESC sampDesc;
  SecureZeroMemory(&sampDesc, sizeof(sampDesc));

  sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
  sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
  sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
  sampDesc.MinLOD = 0;
  sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

  hr = device->CreateSamplerState(&sampDesc, &samplerState);

  assert(SUCCEEDED(hr));

  D3D11_BUFFER_DESC bd;
  SecureZeroMemory(&bd, sizeof(bd));

  bd.Usage = D3D11_USAGE_DEFAULT;
  bd.ByteWidth = sizeof(vertex) * 4;
  bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  bd.CPUAccessFlags = 0;

  hr = device->CreateBuffer(&bd, nullptr, &vertexBuffer);

  assert(SUCCEEDED(hr));

  UINT stride = sizeof(vertex);
  UINT offset = 0;

  context->IASetVertexBuffers( 0, 1, &vertexBuffer, &stride, &offset );
  context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP );

  // Create the texture that the ray tracer will write to
  //
  outputTex = new concurrency::graphics::texture<concurrency::graphics::direct3d::unorm4, 2>(g_rayTracerCam.height, g_rayTracerCam.width, 8U, av);

  ID3D11Texture2D * processedTexture = reinterpret_cast<ID3D11Texture2D *>(concurrency::graphics::direct3d::get_texture<concurrency::graphics::direct3d::unorm4, 2>(*outputTex));

  D3D11_SHADER_RESOURCE_VIEW_DESC shader_view_desc;
  SecureZeroMemory(&shader_view_desc, sizeof(shader_view_desc));

  shader_view_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  shader_view_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
  shader_view_desc.Texture2D.MipLevels = -1;
  shader_view_desc.Texture2D.MostDetailedMip = 0;

   hr = device->CreateShaderResourceView(processedTexture, &shader_view_desc, &outputView);

   assert(SUCCEEDED(hr));

   processedTexture->Release();
}

void CleanUpDirectX(void)
{
  device = 0;
  context = 0;
  swapChain = 0;
  if(vertexBuffer) vertexBuffer->Release();
  vertexLayout = 0;
  vertexShader = 0;
  pixelShader = 0;
  if(samplerState ) samplerState->Release();
  if(outputView) outputView->Release();

  delete outputTex;
}

void DrawFrame(void)
{
  HRESULT hr = S_OK;

  ID3D11Texture2D* pBackBuffer = nullptr;
  hr = swapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );
  assert(SUCCEEDED(hr));

  ID3D11RenderTargetView * pRenderTargetView = nullptr;
  hr = device->CreateRenderTargetView(pBackBuffer, nullptr, &pRenderTargetView);
  assert(SUCCEEDED(hr));

  pBackBuffer->Release();
  context->OMSetRenderTargets( 1, &pRenderTargetView, nullptr );

  D3D11_VIEWPORT vp;
  vp.Width = (FLOAT)g_rayTracerCam.width;
  vp.Height = (FLOAT)g_rayTracerCam.height;
  vp.MinDepth = 0.0f;
  vp.MaxDepth = 1.0f;
  vp.TopLeftX = 0;
  vp.TopLeftY = 0;
  context->RSSetViewports( 1, &vp );

   // Full screen quad
  //
  float h_offset = 1.0f;
  float w_offset = 1.0f;
  float d_offset = 0.1f;
  vertex vertices[] =
  {
    { concurrency::graphics::direct3d::float3( w_offset,  h_offset, d_offset), concurrency::graphics::direct3d::float2(1.0f, 0.0f) },
    { concurrency::graphics::direct3d::float3( w_offset, -h_offset, d_offset), concurrency::graphics::direct3d::float2(1.0f, 1.0f) },
    { concurrency::graphics::direct3d::float3(-w_offset,  h_offset, d_offset), concurrency::graphics::direct3d::float2(0.0f, 0.0f) },
    { concurrency::graphics::direct3d::float3(-w_offset, -h_offset, d_offset), concurrency::graphics::direct3d::float2(0.0f, 1.0f) },
  };

  context->UpdateSubresource(vertexBuffer, 0, nullptr, vertices, 0, 0);

  float ClearColor[4] = { 1.0f, 0.0f, 0.0f, 0.0f }; // black 
  context->ClearRenderTargetView(pRenderTargetView, ClearColor);

  context->VSSetShader( vertexShader, nullptr, 0 );
  context->PSSetShader( pixelShader, nullptr, 0 );

  // Set texture to shader here
  //
  context->PSSetShaderResources(0, 1, &outputView);

  context->PSSetSamplers( 0, 1, &samplerState);
  context->Draw(4, 0);

  context->OMSetRenderTargets(NULL, NULL, NULL);

  swapChain->Present( 0, 0 );

  ID3D11ShaderResourceView * emptySRV = nullptr;
  context->PSSetShaderResources(0, 1, &emptySRV);
  if (pRenderTargetView) pRenderTargetView->Release();

}

#include "../MaterialDatabase/MaterialDB.h"
void CreateBlocks(int rows, int columns)
{
  // ---->
  // |
  // |
  // V
  float startX = -(static_cast<float>(columns) / 2.0f) * 100.0f;

  Point3GPU currentPos(startX, -400.0f, 300.0f);
  for(int y = 0; y < rows; ++y)
  {
    for(int x = 0; x < columns; ++x)
    {
      ObjectGPU obj;
      Mesh mesh;
      mesh.radius = 40.0f;
      mesh.pos = currentPos;
      obj.shape = mesh;
      if((x + y) % 2 == 0)
        obj.material = *MaterialDatabase::GetMaterial("Blue");
      else
        obj.material = *MaterialDatabase::GetMaterial("Green");
      obj.type = Brick;

      g_GPUScene.push_back(obj);

      currentPos.x += 100.0f;
    }

    currentPos.x = startX;
    currentPos.y += 100.0f;
  }
}

void CreateLives(void)
{
  Point3GPU currentPos(-550.0f, -350.0f, 300.0f);
  for(int i = 0; i < 4; ++i)
  {
    ObjectGPU obj;
      Mesh mesh;
      mesh.radius = 20.0f;
      mesh.pos = currentPos;
      obj.shape = mesh;
      obj.material = *MaterialDatabase::GetMaterial("Ball");
      obj.type = Life;

      g_GPUScene.push_back(obj);

      currentPos.y += 50.0f;
  }
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nShowCmd)
{
  open = true;

  SetUpRayTracer();
  InitWindow(hInst);
  InitDirectX();

  CreateBlocks(3, 8);
  CreateLives();

  while(open)
  {
    DWORD startTime = timeGetTime();

    // Update window messages
    //
    MSG msg = {0};
    if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
    
    Update();
    RenderGPU();
    DrawFrame();
    RemoveDeadBricks();

    left = false;
    right = false;
    fire = false;

    // Calculate Frame Rate
    //
    startTime = timeGetTime() - startTime;
    float fps = 1.0f / (static_cast<float>(startTime) / 1000.0f);
    wchar_t buffer[256] = {0};
    swprintf(buffer, sizeof(buffer),L"FPS: %f %d ms", fps, startTime);
    SetWindowText(windowHandle, buffer);
  }

  FreeMemory();
  DestroyWindow(hInst);
  CleanUpDirectX();

  return 0;
}
