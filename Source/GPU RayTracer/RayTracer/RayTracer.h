#pragma once

#include "../Camera/Camera.h"
#include "../Object/Object.h"
#include "../Light/Light.h"
#include <vector>

#ifdef DXRT
#include <D3DX11.h>
#include <DxErr.h>
#include <atlcomcli.h>
#include <amp_graphics.h>

#ifdef _DEBUG
#pragma comment(lib, "d3dx11d.lib")
#pragma comment(lib, "Dxerr.lib")
#else
#pragma comment(lib, "d3dx10.lib")
#endif
#endif

namespace tinyxml2
{
  class XMLElement;
}

class wxProgressDialog;

//class RayTracer
//{
//
//public:
//  RayTracer(void);
//  ~RayTracer(void);
//
//  Camera& GetCamera(void);
//  void Render(wxProgressDialog* progress); 
//  void RenderRecursive(wxProgressDialog* progress);
//  void Reload(void);
//  void LoadScene(const char* filename);
//
//private:
//  void LoadObject(tinyxml2::XMLElement* element);
//  void LoadLight(tinyxml2::XMLElement* element);
//  void FreeMemory(void);
//  void LoadSettings(const char* settingsFilename);
//  void LoadRayTracer(const char* fileName);
//  Color CastRay(Ray& r, int depth);
//  Color LocalLighting(std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay);
//  Color ComputeColor(Ray& r, std::vector<Light*>::iterator light,std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay);
//
//  Camera cam;
//  int recursionDepth;
//  std::vector<Object*> scene;
//  std::vector<Light*> lights;
//};

// Global functions for rendering on the GPU
//
void RenderGPU(void);
void RenderRecursive(void);

// Helper Functions
//
void SetUpRayTracer(void);
void Reload(void);
void LoadScene(const char* filename);
void LoadObject(tinyxml2::XMLElement* element);
void LoadLight(tinyxml2::XMLElement* element);
void FreeMemory(void);
void LoadSettings(const char* settingsFilename);
void LoadRayTracer(const char* fileName);
Color CastRay(Ray& r, int depth);
Color LocalLighting(std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay);
Color ComputeColor(Ray& r, std::vector<Light*>::iterator light,std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay);
void LoadSceneGPU(const char* filename);
void Update(void);
ObjectGPU& FindObjectType(ObjectType type);
void TestBallCollision(ObjectGPU& ball);
bool TestWin(void);
void RemoveDeadBricks(void);

// Globals
//
extern Camera g_rayTracerCam;
extern int g_rayTracerRecursionDepth;
extern std::vector<Object*> g_rayTracerScene;
extern std::vector<Light*> g_rayTracerLights;
extern std::vector<ObjectGPU> g_GPUScene;
extern std::vector<LightGPU> g_GPULights;
extern Color* imagePlaneCPU;
extern concurrency::accelerator_view av;
extern concurrency::graphics::texture<concurrency::graphics::direct3d::unorm4, 2>* outputTex;