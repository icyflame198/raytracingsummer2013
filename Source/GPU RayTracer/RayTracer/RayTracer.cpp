#include "RayTracer.h"

#include "../Math/Ray/Ray.h"

#include "../Shape/Sphere/Sphere.h"
#include "../Shape/Plane/Plane.h"
#include "../Shape/Triangle/Triangle.h"
#include "../Shape/Quad/Quad.h"

#include <limits>

#include "../Factory/Factory.h"
#include "../MaterialDatabase/MaterialDB.h"

#include "../TinyXML/tinyxml2.h"

#undef min
#undef max

// Globals
//
Camera g_rayTracerCam;
int g_rayTracerRecursionDepth;
std::vector<Object*> g_rayTracerScene;
std::vector<Light*> g_rayTracerLights;
std::vector<ObjectGPU> g_GPUScene;
std::vector<LightGPU> g_GPULights;
Color* imagePlaneCPU;
concurrency::accelerator_view av = concurrency::accelerator().default_view;
concurrency::graphics::texture<concurrency::graphics::direct3d::unorm4, 2>* outputTex = nullptr;

/*RayTracer(void) : g_rayTracerRecursionDepth(1)
{
// Add shapes to the factory
//
ShapeFactory::RegisterPrototype("Sphere", new TypedPrototype<Sphere>());
ShapeFactory::RegisterPrototype("Plane", new TypedPrototype<Plane>());
ShapeFactory::RegisterPrototype("Triangle", new TypedPrototype<Triangle>());
ShapeFactory::RegisterPrototype("Quad", new TypedPrototype<Quad>());

// This will be replaced by a settings file later
//
LoadSettings("Settings.xml");
g_rayTracerCam.ClearImage(Color(0,0,0));

//g_rayTracerCam.LoadCameraInfo("..//..//Resources//camera.xml");
//MaterialDatabase::LoadFromFile("..//..//Resources//materials.xml");
//LoadScene("..//..//Resources//g_rayTracerScene.xml");
}

~RayTracer(void)
{
// Free all the memory associated with the factory and database
//
ShapeFactory::CleanFactory();
MaterialDatabase::CleanDatabase();
FreeMemory();
}*/

void SetUpRayTracer(void)
{
  // Add shapes to the factory
  //
  ShapeFactory::RegisterPrototype("Sphere", new TypedPrototype<Sphere>());
  ShapeFactory::RegisterPrototype("Plane", new TypedPrototype<Plane>());
  ShapeFactory::RegisterPrototype("Triangle", new TypedPrototype<Triangle>());
  ShapeFactory::RegisterPrototype("Quad", new TypedPrototype<Quad>());

  // This will be replaced by a settings file later
  //
  LoadSettings("Settings.xml");
  g_rayTracerCam.ClearImage(Color(0,0,0));

  imagePlaneCPU = new Color[g_rayTracerCam.height * g_rayTracerCam.width];
}

void RenderRecursive(void)
{
  // These are used for calculating every point on the image plane
  //
  float u = static_cast<float>(g_rayTracerCam.width - 1) / 2.0f;
  float v = static_cast<float>(g_rayTracerCam.height - 1) / 2.0f;

  // This is the center of the image plane
  //
  Point3 C(g_rayTracerCam.imgPos);

  // Loop through every point on the image plane
  //
  for(int y = 0; y < g_rayTracerCam.height; ++y)
  {
    for(int x = 0; x < g_rayTracerCam.width; ++x)
    {
      // Calculate [-1, 1] value to get the current point on the image plane
      //
      float alpha = (static_cast<float>(x) / u) - 1.0f;
      float beta = (static_cast<float>(y) / v) - 1.0f;

      // Calculate our ray from the eye position to the image plane
      //
      Point3 imagePlanePoint = C + Point3(alpha * u, beta * v, 0.0f);
      Ray r(imagePlanePoint - g_rayTracerCam.pos, imagePlanePoint, Eye_Ray);

      Color finalColor = CastRay(r, g_rayTracerRecursionDepth);

      // Set the final pixel color
      //
      g_rayTracerCam.SetPixel(x,y, finalColor);
    }

    // Display to the user the current progress, this will be useful for large images
    //
    int progressStatus = static_cast<int>((static_cast<float>(g_rayTracerCam.width * y) / static_cast<float>(g_rayTracerCam.width * g_rayTracerCam.height)) * 100.0f);
    char buffer[5] = {0};
#pragma warning (disable:4996)
    sprintf(buffer, "%d",progressStatus);
#pragma warning (default:4996)
    //progress->Update(g_rayTracerCam.width * y, buffer);
  }
}

Color CastRay(Ray& r, int depth)
{
  // The final color of the pixel
  //
  Color finalColor;

  // The closest intersection point, as well as the object index of closest intersection
  //
  float lowestT = std::numeric_limits<float>::max();
  std::vector<Object*>::iterator objectIter;

  // Loop through every object in the g_rayTracerScene, check for collision
  //
  for(auto obj = g_rayTracerScene.begin(); obj != g_rayTracerScene.end(); ++obj)
  {
    float intersection;
    if((*obj)->TestIntersection(r, &intersection))
    {
      // Find the nearest collision point
      //
      if(intersection < lowestT)
      {
        lowestT = intersection;
        objectIter = obj;
      }
    }
  }

  // If a intersection was found and is less than 500000 units away, think of this as a far plane
  //
  if(lowestT != std::numeric_limits<float>::max() && lowestT < 50000.0f)
  {
    // If it is here then an intersection has happened
    //
    Point3 interPoint(r.FindPointOnRay(lowestT));
    Vec3 normal = (*objectIter)->shape->FindNormal(interPoint);
    Material* mat = (*objectIter)->material;

    const float epislon = 0.01f;
    // Reflection ray direction = incidentDir - 2 * incidentDir(Dot)normal * normal
    //
    Ray reflectRay(r.direction - (normal * 2.0f * r.direction.Dot(normal)), interPoint, Reflect_Ray);
    reflectRay.origin = reflectRay.origin + (reflectRay.direction * epislon);
    Color materialLightColor = LocalLighting(objectIter, interPoint, normal, reflectRay);

    // Add the final color of the point using phong lighting
    //
    if(depth == 0 || mat->reflectivity == 0.0f)
    {
      finalColor = finalColor + materialLightColor;
    }
    else
    {
      Color reflectColor = CastRay(reflectRay, depth - 1) * mat->reflectivity;
      finalColor = finalColor + materialLightColor + reflectColor;
    }
  }
  else
  {
    // No collision occured set the color to grey, like a clear color
    //
    finalColor.SetColor(0.5f, 0.5f, 0.5f);
  }

  return finalColor;
}

Color LocalLighting(std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay)
{
  Color finalColor;

  // Run through every light in the g_rayTracerScene and apply lighting
  //
  for(auto light = g_rayTracerLights.begin(); light != g_rayTracerLights.end(); ++light)
  {
    // TODO Make this work for more than just undirected point g_rayTracerLights
    //

    // Create a light ray from the point to the light
    //
    Ray lightRay((*light)->pos - interPoint, interPoint, Light_Ray);

    // This is the radius that the light will be considered as
    //
    const float radius = 5.0f;

    // TODO Make this more modular to specifiy the number of samples per light
    //

    // 7 Points on the light, use the power as the radius
    //
    Point3 left = (*light)->pos + (Vec3(-1.0f, 0.0f, 0.0f) * radius);
    Point3 right = (*light)->pos + (Vec3(1.0f, 0.0f, 0.0f) * radius);

    Point3 up = (*light)->pos + (Vec3(0.0f, 1.0f, 0.0f) * radius);
    Point3 down = (*light)->pos + (Vec3(0.0f, -1.0f, 0.0f) * radius);

    Point3 forward = (*light)->pos + (Vec3(0.0f, 0.0f, 1.0f) * radius);
    Point3 backward = (*light)->pos + (Vec3(0.0f, 0.0f, -1.0f) * radius);

    // Cast rays to these points and get the local illumination for each
    //
    Ray l(left - interPoint, interPoint, Light_Ray);
    Ray r(right - interPoint, interPoint, Light_Ray);
    Ray u(up - interPoint, interPoint, Light_Ray);
    Ray d(down - interPoint, interPoint, Light_Ray);
    Ray f(forward - interPoint, interPoint, Light_Ray);
    Ray b(backward - interPoint, interPoint, Light_Ray);

    Color lightRayColor = ComputeColor(lightRay, light, objectIter, interPoint, normal, reflectRay);
    if(lightRay.type == Shadow_Ray)
    {
      finalColor = finalColor + ( lightRayColor * (1.0f / 7.0f) +
        ComputeColor(l, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
        ComputeColor(r, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
        ComputeColor(u, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
        ComputeColor(d, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
        ComputeColor(f, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f) +
        ComputeColor(b, light, objectIter, interPoint, normal, reflectRay) * (1.0f / 7.0f));
    }
    else
    {
      finalColor = finalColor + lightRayColor;
    }
  }

  return finalColor;
}

Color ComputeColor(Ray& r, std::vector<Light*>::iterator light, std::vector<Object*>::iterator objectIter, Point3& interPoint, Vec3& normal, Ray& reflectRay)
{
  Material* mat = (*objectIter)->material;
  float lowestT = std::numeric_limits<float>::max();

  // If the dot product between the normal and light direction it means the light is facing away
  //
  if(normal.Dot(r.direction) <= 0.0f)
  {
    return Color();
  }

  // Reset lowestT to avoid collision errors
  //
  lowestT = std::numeric_limits<float>::max();

  // Check if there is an opaque object between the object and the light
  //
  for(auto obj = g_rayTracerScene.begin(); obj != g_rayTracerScene.end(); ++obj)
  {
    // Don't compare the object with it self
    //
    if(obj == objectIter)
      continue;

    float intersection;
    if((*obj)->TestIntersection(r, &intersection))
    {
      // Find the nearest collision point
      //
      lowestT = std::min(intersection, lowestT);
    }
  }

  float maxT = ((*light)->pos - interPoint).Length();

  // If a collision happened and it is between the object and the light
  //
  if(lowestT != std::numeric_limits<float>::max() && lowestT < maxT)
  {
    r.type = Shadow_Ray;
    return Color();
  }

  // Calculate the amount of radiance for the point, using diffuse term
  //
  float diffuse = (1.0f - mat->reflectivity) * normal.Dot(r.direction) * (*light)->power;

  // Specular term is just mat->reflectivity
  //
  Vec3 rl = g_rayTracerCam.pos - interPoint;
  rl.Normalize();
  float specular = std::max(mat->reflectivity * std::pow(rl.Dot(reflectRay.direction), 4), 0.0f) * (*light)->power;

  // Calcualte local lighting using phong lighting
  //
  Color currentColor;// = (mat->color * diffuse);
  if(mat->isTex)
  {
    float u, v;
    (*objectIter)->shape->GetTexCoords(&u, &v);
    currentColor = (mat->GetTexColor(u,v) * diffuse);
  }
  else
    currentColor = (mat->color * diffuse);

  currentColor = currentColor + (currentColor * specular);
  return currentColor;
}

void LoadScene(const char* filename)
{
  tinyxml2::XMLDocument doc;

  // If file loaded successfully
  //
  if(doc.LoadFile(filename) == tinyxml2::XML_NO_ERROR)
  {
    for(tinyxml2::XMLElement* element = doc.FirstChildElement(); element != NULL; element = element->NextSiblingElement())
    {
      if(strcmp(element->Value(), "Object") == 0)
      {
        LoadObject(element);
      }
      else if(strcmp(element->Value(), "Light") == 0)
      {
        LoadLight(element);
      }
    }
  }
  else
  {
    // TODO proper error handling
    //
  }
}

void LoadObject(tinyxml2::XMLElement* element)
{
  Object* obj = new Object;
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Shape") == 0)
    {
      Shape* newShape = reinterpret_cast<Shape*>(ShapeFactory::shapes[it->Attribute("type")]->CreateType());
      newShape->Import(it);
      obj->shape = newShape;
    }
    else if(strcmp(it->Value(), "Material") == 0)
    {
      obj->material = MaterialDatabase::GetMaterial(it->Attribute("name"));
    }
  }

  g_rayTracerScene.push_back(obj);
}

void LoadLight(tinyxml2::XMLElement* element)
{
  Light* l = new Light;

  int type;
  element->QueryIntAttribute("type", &type);
  l->type = static_cast<LightType>(type);

  element->QueryFloatAttribute("posX", &l->pos.x);
  element->QueryFloatAttribute("posY", &l->pos.y);
  element->QueryFloatAttribute("posZ", &l->pos.z);

  element->QueryFloatAttribute("dirX", &l->dir.x);
  element->QueryFloatAttribute("dirY", &l->dir.y);
  element->QueryFloatAttribute("dirZ", &l->dir.z);

  int red, green, blue;

  element->QueryIntAttribute("red", &red);
  element->QueryIntAttribute("green", &green);
  element->QueryIntAttribute("blue", &blue);

  l->lightColor.SetColor(red / 255.0f, green / 255.0f, blue / 255.0f);

  element->QueryFloatAttribute("power", &l->power);

  g_rayTracerLights.push_back(l);
}

void Reload(void)
{
  MaterialDatabase::CleanDatabase();
  FreeMemory();

  LoadSettings("Settings.xml");

  // The data on the CPU that the array view will write to
  //
  imagePlaneCPU = new Color[g_rayTracerCam.height * g_rayTracerCam.width];
}

void FreeMemory(void)
{
  for(auto it = g_rayTracerScene.begin(); it != g_rayTracerScene.end(); ++it)
  {
    delete (*it);
  }

  g_rayTracerScene.clear();

  for(auto it = g_rayTracerLights.begin(); it != g_rayTracerLights.end(); ++it)
  {
    delete (*it);
  }

  g_rayTracerLights.clear();

  g_GPUScene.clear();
  g_GPULights.clear();

  tris.clear();

  if(imagePlaneCPU)
  {
    delete[] imagePlaneCPU;
  }

  // TODO fix the texture to be the right size here, don't forget, it will break...
  //
}

void LoadSettings(const char* settingsFilename)
{
  tinyxml2::XMLDocument doc;

  // If file loaded successfully
  //
  if(doc.LoadFile(settingsFilename) == tinyxml2::XML_NO_ERROR)
  {
    tinyxml2::XMLElement* root = doc.FirstChildElement();

    for(tinyxml2::XMLElement* element = root; element != NULL; element = element->NextSiblingElement())
    {
      const char* name = element->Value();
      const char* fileName = element->GetText();

      // TODO Make this faster than all of the string comparisions
      //
      if(strcmp(name, "Camera") == 0)
      {
        g_rayTracerCam.LoadCameraInfo(fileName);
      }
      else if(strcmp(name, "Materials") == 0)
      {
        MaterialDatabase::LoadFromFile(fileName);
      }
      else if(strcmp(name, "Scene") == 0)
      {
        LoadSceneGPU(fileName);
      }
      else if(strcmp(name, "RayTracer") == 0)
      {
        LoadRayTracer(fileName);
      }
    }

    // Free the memory used by the document
    //
    doc.Clear();
  }
  else
  {
    // TODO The file failed to load need some error handling here
    //
  }
}

void LoadRayTracer(const char* filename)
{
  tinyxml2::XMLDocument doc;

  // If file loaded successfully
  //
  if(doc.LoadFile(filename) == tinyxml2::XML_NO_ERROR)
  {
    tinyxml2::XMLElement* root = doc.FirstChildElement();

    for(tinyxml2::XMLElement* element = root; element != NULL; element = element->NextSiblingElement())
    {
      const char* name = element->Value();

      // TODO Make this faster than all of the string comparisions
      //
      if(strcmp(name, "RayTracer") == 0)
      {
        element->QueryIntAttribute("depth", &g_rayTracerRecursionDepth);
      }
    }

    // Free the memory used by the document
    //
    doc.Clear();
  }
  else
  {
    // TODO The file failed to load need some error handling here
    //
  }
}

#include <amp.h>
#include <amp_math.h>

void LoadLightGPU(tinyxml2::XMLElement* element)
{
  LightGPU l;

  int type;
  element->QueryIntAttribute("type", &type);
  //l.type = static_cast<LightType>(type);

  element->QueryFloatAttribute("posX", &l.pos.x);
  element->QueryFloatAttribute("posY", &l.pos.y);
  element->QueryFloatAttribute("posZ", &l.pos.z);

  //element->QueryFloatAttribute("dirX", &l.dir.x);
  //element->QueryFloatAttribute("dirY", &l->dir.y);
  //element->QueryFloatAttribute("dirZ", &l->dir.z);

  int red, green, blue;

  element->QueryIntAttribute("red", &red);
  element->QueryIntAttribute("green", &green);
  element->QueryIntAttribute("blue", &blue);

  //l.lightColor.SetColor(red / 255.0f, green / 255.0f, blue / 255.0f);

  element->QueryFloatAttribute("power", &l.power);

  g_GPULights.push_back(l);
}

void LoadObjectGPU(tinyxml2::XMLElement* element)
{
  ObjectGPU obj;
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Shape") == 0)
    {
      Mesh mesh;
      mesh.Import(it);
      obj.shape = mesh;
    }
    else if(strcmp(it->Value(), "Material") == 0)
    {
      obj.material = *MaterialDatabase::GetMaterial(it->Attribute("name"));
    }
    else if(strcmp(it->Value(), "GameLogic") == 0)
    {
      int type = static_cast<int>(Other);
      it->QueryIntAttribute("type", &type);
      
      switch(type)
      {
      case Player:
        obj.type = Player;
        break;
        
      case Ball:
        obj.type = Ball;
        obj.status = Ball_Start;
        break;

      case Brick:
        obj.type = Brick;
        break;

      default:
        obj.type = Other;
        break;
      }
    }
  }

  g_GPUScene.push_back(obj);
}

void LoadSceneGPU(const char* filename)
{
  tinyxml2::XMLDocument doc;

  // If file loaded successfully
  //
  if(doc.LoadFile(filename) == tinyxml2::XML_NO_ERROR)
  {
    for(tinyxml2::XMLElement* element = doc.FirstChildElement(); element != NULL; element = element->NextSiblingElement())
    {
      if(strcmp(element->Value(), "Object") == 0)
      {
        LoadObjectGPU(element);
      }
      else if(strcmp(element->Value(), "Light") == 0)
      {
        LoadLightGPU(element);
      }
    }
  }
  else
  {
    // TODO proper error handling
    //
  }
}

Color ComputeColorGPU(RayGPU& r, 
                      int light,
                      concurrency::array<LightGPU, 1>& lights,
                      int object,
                      concurrency::array<ObjectGPU, 1>& scene,
                      int sceneSize,
                      Point3GPU& interPoint,
                      Vec3GPU& normal,
                      RayGPU& reflectRay,
                      Point3GPU& camPos, 
                      concurrency::array<Tri, 1>& models) restrict(amp)
{
  // If the object is shadowing its self
  //
  if(normal.Dot(r.direction) <= 0.0f)
  {
    return Color();
  }

  // Used to find the closest intersecton time
  //
  float lowestT = 100000.0f;
  float t;
  
  for(int i = 0; i < sceneSize; ++i)
  {
    if(i == object)
      continue;

    if(scene[i].TestIntersection(r, t, models))
    {
      // Find nearest collision time
      //
      lowestT = concurrency::fast_math::fmin(lowestT, t);
    }
  }

  // The furthest distance from the point to the light, no objects can cause shadows past this point
  //
  float maxT = (lights[light].pos - interPoint).Length();

  // If there is a collision with an object between the light and object this is a shadow
  //
  if(lowestT < 100000.0f && lowestT < maxT)
  {
    return Color();
  }

  // Calculate the amount of radiance for the point, using diffuse term
  //
  float diffuse = (1.0f - scene[object].material.reflectivity) * normal.Dot(r.direction) * lights[light].power;

  // Specular term is just mat->reflectivity
  //
  Vec3GPU rl = camPos - interPoint;
  rl.Normalize();
  float specular = concurrency::fast_math::fmax(scene[object].material.reflectivity * concurrency::fast_math::pow(rl.Dot(reflectRay.direction), 19), 0.0f);

  // Calculate the final color using Phong lighting
  //
  Color currentColor = scene[object].material.color * diffuse;
  currentColor = currentColor + (Color(1.0f,1.0f,1.0f) * specular);// (currentColor * specular);//<< Should be this

  return currentColor;
}

Color LocalLightingGPU(int object, 
                       Point3GPU& interPoint, 
                       Vec3GPU& normal, 
                       RayGPU& reflectRay, 
                       concurrency::array<ObjectGPU, 1>& scene, 
                       int sceneSize, 
                       concurrency::array<LightGPU, 1>& lights, 
                       int lightsSize,
                       Point3GPU& camPos,
                       concurrency::array<Tri, 1>& models) restrict(amp)
{
  Color finalColor;

  for(int i = 0; i < lightsSize; ++i)
  {
    // Create a ray from the object to the light
    //
    RayGPU lightRay(lights[i].pos - interPoint, interPoint, Light_Ray);

    // This is the radius that the light will be considered as
    //
    const float radius = 5.0f;

    // TODO Make this more modular to specifiy the number of samples per light
    //

    // 7 Points on the light, use the power as the radius
    //
    Point3GPU left = lights[i].pos + (Vec3GPU(-1.0f, 0.0f, 0.0f) * radius);
    Point3GPU right = lights[i].pos + (Vec3GPU(1.0f, 0.0f, 0.0f) * radius);

    Point3GPU up = lights[i].pos + (Vec3GPU(0.0f, 1.0f, 0.0f) * radius);
    Point3GPU down = lights[i].pos + (Vec3GPU(0.0f, -1.0f, 0.0f) * radius);

    Point3GPU forward = lights[i].pos + (Vec3GPU(0.0f, 0.0f, 1.0f) * radius);
    Point3GPU backward = lights[i].pos + (Vec3GPU(0.0f, 0.0f, -1.0f) * radius);

    // Cast rays to these points and get the local illumination for each
    //
    RayGPU l(left - interPoint, interPoint, Light_Ray);
    RayGPU r(right - interPoint, interPoint, Light_Ray);
    RayGPU u(up - interPoint, interPoint, Light_Ray);
    RayGPU d(down - interPoint, interPoint, Light_Ray);
    RayGPU f(forward - interPoint, interPoint, Light_Ray);
    RayGPU b(backward - interPoint, interPoint, Light_Ray);

    // The coefficent of how much each ray contributes to the final color
    //
    const float coeff = 1.0f / 7.0f;

    // Compute the color of each point by sending 7 rays per point
    //
    finalColor = finalColor + 
      ComputeColorGPU(lightRay, i, lights, object, scene, sceneSize, interPoint, normal, reflectRay, camPos, models)/* * coeff
      + ComputeColorGPU(l, i, lights, object, scene, sceneSize, interPoint, normal, reflectRay, camPos, models) * coeff
      + ComputeColorGPU(r, i, lights, object, scene, sceneSize, interPoint, normal, reflectRay, camPos, models) * coeff
      + ComputeColorGPU(u, i, lights, object, scene, sceneSize, interPoint, normal, reflectRay, camPos, models) * coeff
      + ComputeColorGPU(d, i, lights, object, scene, sceneSize, interPoint, normal, reflectRay, camPos, models) * coeff
      + ComputeColorGPU(f, i, lights, object, scene, sceneSize, interPoint, normal, reflectRay, camPos, models) * coeff
      + ComputeColorGPU(b, i, lights, object, scene, sceneSize, interPoint, normal, reflectRay, camPos, models) * coeff*/;
    // ^^^ Soft shadowing is currently removed due to an issue where the data would not copy properly
  }

  return finalColor;
}

Color CastRayGPU(RayGPU& r, 
                 concurrency::array<ObjectGPU, 1>& scene, 
                 int sceneSize, 
                 concurrency::array<LightGPU, 1>& lights, 
                 int lightSize, 
                 Point3GPU& camPos,
                 concurrency::array<Tri,1>& models,
                 int& collisionIndex,
                 RayGPU& reflect) restrict(amp)
{
  // Used to find the closest intersection point
  //
  float lowestT = 100000.0f;

  // Which object the closest intersection point corresponds to
  //
  int closestObject;

  // A temp that is used as the intersection time
  //
  float t;
  Color finalColor;
  // Loop through the entire scene looking for which objects collide
  //
  for(int i = 0; i < sceneSize; ++i)
  {
    if(scene[i].TestIntersection(r, t, models))
    {   
      // If the collision is closer than the previous known collision
      //
      if(t < lowestT)
      {
        lowestT = t;
        closestObject = i;
      }
    }
  }

  if(lowestT < 100000.0f)
  {
    Point3GPU interPoint(r.FindPointOnRay(lowestT));
    Vec3GPU normal = scene[closestObject].shape.FindNormal(interPoint, models);

     const float epislon = 0.01f;
    // Reflection ray direction = incidentDir - 2 * incidentDir(Dot)normal * normal
    //
    RayGPU reflectRay(r.direction - (normal * 2.0f * r.direction.Dot(normal)), interPoint, Reflect_Ray);
    reflectRay.origin = reflectRay.origin + (reflectRay.direction * epislon);

    finalColor = LocalLightingGPU(closestObject, interPoint, normal, reflectRay, scene, sceneSize, lights, lightSize, camPos, models);
    collisionIndex = closestObject;
    reflect = reflectRay;
  }
  

  return finalColor;
}
#include <iostream>
void RenderGPU(void)       
{
  // The array view that will communicate with the GPU
  //
  //concurrency::array_view<Color, 2> imagePlaneAV(g_rayTracerCam.height, g_rayTracerCam.width,imagePlaneCPU);

  // Call discard data since we are only writing to this buffer there is no reason to copy garbage data to the GPU
  //
  //imagePlaneAV.discard_data();

  tris.push_back(Tri());

  // NOTE! The scene and lights CANNOT! be empty! It will crash!
  //

  // Create an array the GPU can access for the scene and lights
  //
  concurrency::array<ObjectGPU, 1> scene(g_GPUScene.size(),g_GPUScene.begin(), g_GPUScene.end(), outputTex->get_accelerator_view());
  concurrency::array<LightGPU, 1> lights(g_GPULights.size(), g_GPULights.begin(), g_GPULights.end(), outputTex->get_accelerator_view());

  // Size of the scene and light arrays
  //
  int sceneSize = g_GPUScene.size();
  int lightSize = g_GPULights.size();

  concurrency::array<Tri, 1> models(tris.size(), tris.begin(), tris.end(), outputTex->get_accelerator_view());

  // These are used for calculating every point on the image plane
  //
  float u = static_cast<float>(g_rayTracerCam.width - 1) / 2.0f;
  float v = static_cast<float>(g_rayTracerCam.height - 1) / 2.0f;

  // This is the center of the image plane
  //
  Point3 C(g_rayTracerCam.imgPos);

  // Camera Pos
  //
  float camX = g_rayTracerCam.pos.x;
  float camY = g_rayTracerCam.pos.y;
  float camZ = g_rayTracerCam.pos.z;

  concurrency::graphics::writeonly_texture_view<concurrency::graphics::direct3d::unorm4, 2> output_tex_view(*outputTex);

  int maxBounces = g_rayTracerRecursionDepth;

  /*Point3GPU tri0P0 = tris[0].p0;
  Point3GPU tri0P1 = tris[0].p1;
  Point3GPU tri0P2 = tris[0].p2;

  Point3GPU tri1P0 = tris[1].p0;
  Point3GPU tri1P1 = tris[1].p1;
  Point3GPU tri1P2 = tris[1].p2;*/

  // Run the Ray Tracer in parallel for each pixel on the image plane
  //
  concurrency::parallel_for_each(outputTex->get_accelerator_view(), output_tex_view.extent, [=, &scene, &lights, &models](concurrency::index<2> idx) restrict(amp)
  {
    // X and Y position on the image plane
    //
    int y = idx[0];
    int x = idx[1];

    // Calculate [-1, 1] value to get the current point on the image plane
    //
    float alpha = (static_cast<float>(x) / u) - 1.0f;
    float beta = (static_cast<float>(y) / v) - 1.0f;

    // Calculate our ray from the eye position to the image plane
    //
    RayGPU eyeRay;
    eyeRay.origin.x = C.x + (alpha * u);
    eyeRay.origin.y = C.y + (beta * v);
    eyeRay.origin.z = C.z;

    Point3GPU camPos(camX, camY, camZ);

    // Calculate Ray Direction
    //
    eyeRay.direction = eyeRay.origin - camPos;
    //eyeRay.direction = Vec3GPU(0.0f, 0.0f, 1.0f); // Orthographic

    // Normalize the direction
    //
    eyeRay.direction.Normalize();

    int collisionIndex = -1;
    float contribution = 1.0f;

    // Cast the ray and get the final color
    //
    Color c;

    for(int i = 0; i < maxBounces; ++i)
    {
      Color temp = CastRayGPU(eyeRay, scene, sceneSize, lights, lightSize, camPos, models, collisionIndex, eyeRay);

      if(collisionIndex == -1)
      {
        break;
      }
      else
      {
        c = c + (temp * contribution);
        contribution *= scene[collisionIndex].material.reflectivity;
      }

      collisionIndex = -1;
    }

    // Thought this would be a good test to check that everything is copying over correctly
    //
    /*Color c = Color(1.0f, 1.0f, 1.0f);

    if(tri0P0.x != models[0].p0.x || tri0P0.y != models[0].p0.y || tri0P0.z != models[0].p0.z ||
      tri0P1.x != models[0].p1.x || tri0P1.y != models[0].p1.y || tri0P1.z != models[0].p1.z ||
      tri0P2.x != models[0].p2.x || tri0P2.y != models[0].p2.y || tri0P2.z != models[0].p2.z)
      c = Color(1.0f, 0.0f, 0.0f);

    if(tri1P0.x != models[1].p0.x || tri1P0.y != models[1].p0.y || tri1P0.z != models[1].p0.z ||
      tri1P1.x != models[1].p1.x || tri1P1.y != models[1].p1.y || tri1P1.z != models[1].p1.z ||
      tri1P2.x != models[1].p2.x || tri1P2.y != models[1].p2.y || tri1P2.z != models[1].p2.z)
      c = c + Color(0.0f, 1.0f, 0.0f);*/

    // Set the final color
    //
    output_tex_view.set(idx, concurrency::graphics::direct3d::unorm4(c.red, c.green, c.blue, 1.0f));

  });

  // Wait for the GPU to finish rendering then copy the data back to the CPU
  //
  //imagePlaneAV.synchronize();

  // Copy the data to the image plane that will be displayed using wxWidgets
  //
  //g_rayTracerCam.CopyImage(imagePlaneCPU);
}

void Update(void)
{
  for(auto i = g_GPUScene.begin(); i != g_GPUScene.end(); ++i)
  {
    i->Update();
  }

  for(auto i = g_GPULights.begin(); i != g_GPULights.end(); ++i)
  {
    i->Update();
  }
}

ObjectGPU& FindObjectType(ObjectType type)
{
  for(auto i = g_GPUScene.begin(); i != g_GPUScene.end(); ++i)
  {
    if(i->type == type)
      return *i;
  }

  return ObjectGPU();
}

void TestBallCollision(ObjectGPU& ball)
{
  for(auto i = g_GPUScene.begin(); i != g_GPUScene.end(); ++i)
  {
    if(i->type == Brick && i->status != Brick_Dead)
    {
      if((ball.shape.pos - i->shape.pos).Length() <= (ball.shape.radius + i->shape.radius))
        i->status = Brick_Collided;
    }
  }
}

bool TestWin(void)
{
  for(auto i = g_GPUScene.begin(); i != g_GPUScene.end(); ++i)
  {
    if(i->type == Brick && i->status != Brick_Dead)
    {
      return false;
    }
  }

  return true;
}

void RemoveDeadBricks(void)
{
  auto i = g_GPUScene.begin();
  for(; i != g_GPUScene.end(); ++i)
  {
    if(i->type == Brick && i->status == Brick_Dead)
    {
      g_GPUScene.erase(i);
      break;
    }
  }
}