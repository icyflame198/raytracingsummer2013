#include "../RayTracer/RayTracer.h"
#include <iostream>
#include <fstream>
#include <mmsystem.h>

void SaveAsPNG(const char* filename, const char* outputDir)
{
  char buffer[1024] = {0};
  sprintf_s(buffer, sizeof(buffer), "%s\\%s", outputDir, filename);
  std::ofstream outFile(buffer);

  if(outFile.good())
  {
    outFile << "P3\n";
    outFile << "# Created by David James\n";
    outFile << g_rayTracerCam.width << ' ' << g_rayTracerCam.height << '\n';
    outFile << 255 << '\n';

    for(int y = 0; y < g_rayTracerCam.height; ++y)
    {
      for(int x = 0; x < g_rayTracerCam.width; ++x)
      {
        outFile << static_cast<int>(imagePlaneCPU[(y * g_rayTracerCam.width) + x].red * 255.0f) << ' '
          << static_cast<int>(imagePlaneCPU[(y * g_rayTracerCam.width) + x].green * 255.0f) << ' '
          << static_cast<int>(imagePlaneCPU[(y * g_rayTracerCam.width) + x].blue * 255.0f) << ' ';
      }
      outFile << '\n';
    }

    outFile.close();
  }
}

void Init(void)
{
  SetUpRayTracer();
}

int main(void)
{
  Init();

  int status;
  int frameCount = 0;
  do
  {
    std::cout << "Ray Tracer Version 0.3\nAuthor: David James" << std::endl;

    std::cout << "0 - Exit\n1 - Render Frame\n2 - Reload Files" << std::endl;
    std::cin >> status;

    switch (status)
    {
    case 0:
      break;

    case 1:
      {
        DWORD startTime = timeGetTime();
        RenderGPU();
        startTime = timeGetTime() - startTime;
        std::cout << "Rendered frame in: " << static_cast<float>(startTime) / 1000.0f << "s\n";
        char buffer[128] = {0};
        sprintf_s(buffer, sizeof(buffer), "RayTracerFrame_%d.ppm", frameCount);
        ++frameCount;
        SaveAsPNG(buffer, "C:\\Users\\David\\Downloads\\ImageMagick-6.8.6-5-Q16-x86-windows\\ImageMagick-6.8.6-5");
        //SaveAsPNG(buffer, ".");
      }
      break;

    case 2:
      Reload();
      break;

    default:
      std::cout << "Did not recongnize that command...\n";
      break;
    }
  }while(status != 0);

  FreeMemory();
  return 0;
}