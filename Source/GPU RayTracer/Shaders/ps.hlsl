struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD0;
};
struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
};

Texture2D<unorm float4> txDiffuse : register( t0 );
SamplerState samp : register( s0 );
float4 main( PS_INPUT input) : SV_Target
{
    return txDiffuse.Sample( samp, input.Tex );
}