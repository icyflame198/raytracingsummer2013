#pragma once 

#include "../Math/Ray/Ray.h"
#include "../Material/Material.h"
#include "../Shape/TriMesh/TriMesh.h"

class Shape;

class Object
{
public:
  Object(void);
  Object(const Object& rhs);
  Object& operator=(const Object& rhs);
  ~Object(void);

  bool TestIntersection(Ray& r, float* intersectionT);

  Shape* shape;
  Material* material;
private:
  

};

enum ObjectType
{
  Player = 0,
  Ball,
  Brick,
  Life,
  Other
};

enum StatusTypes
{
  Ball_Start = 0,
  Ball_Flight,
  Brick_Collided,
  Brick_Dead,
  Brick_Shrink,
  None
};

struct ObjectGPU
{
  ObjectGPU(void) restrict(cpu, amp);
  ObjectGPU(const ObjectGPU& rhs) restrict(cpu, amp);
  ObjectGPU& operator=(const ObjectGPU& rhs) restrict(cpu, amp);
  ~ObjectGPU(void) restrict(cpu, amp);

  void Update(void) restrict(cpu);

  bool TestIntersection(RayGPU& r, float& intersectionT, concurrency::array<Tri, 1>& models) const restrict(amp);

  Mesh shape;
  MaterialGPU material;

  ObjectType type;
  StatusTypes status;

  Vec3GPU direction;

  float prevRadius;
};

extern bool left, right, fire;
extern int lives;