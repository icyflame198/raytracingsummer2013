#include "Object.h"

#include "../Shape/Parent/Shape.h"
#include "../Factory/Factory.h"
#include "../MaterialDatabase/MaterialDB.h"

#include "../RayTracer/RayTracer.h"

#include <amp.h>

bool left = false;
bool right = false;
bool fire = false;
int lives = 5;

Object::Object(void) : shape(NULL)
{
}

Object::Object(const Object& rhs)
{
  // TODO Make a function that takes a name and returns an object
  //
  shape = reinterpret_cast<Shape*>(ShapeFactory::shapes[rhs.shape->typeName]->CreateType());
}

Object& Object::operator=(const Object& rhs)
{
  // TODO Make a function that takes a name and returns an object
  //
  shape = reinterpret_cast<Shape*>(ShapeFactory::shapes[rhs.shape->typeName]->CreateType());

  return *this;
}

Object::~Object(void)
{
  if(shape)
  {
    delete shape;
  }
}

bool Object::TestIntersection(Ray& r, float* intersectionT)
{
  return shape->TestIntersection(r, intersectionT);
}

// GPU Object code is here
//
ObjectGPU::ObjectGPU(void) restrict(cpu, amp)
{
  type = Other;
}

ObjectGPU::ObjectGPU(const ObjectGPU& rhs) restrict(cpu, amp)
{
  // TODO Make this a deep copy
  //
  shape = rhs.shape;
  material = rhs.material;
  type = rhs.type;
  status = rhs.status;
  direction = rhs.direction;
}

ObjectGPU& ObjectGPU::operator=(const ObjectGPU& rhs) restrict(cpu, amp)
{
  // TODO Make a better copy
  //
  shape = rhs.shape;
  material = rhs.material;
  type = rhs.type;
  status = rhs.status;
  direction = rhs.direction;

  return *this;
}

ObjectGPU::~ObjectGPU(void) restrict(cpu, amp)
{

}

bool ObjectGPU::TestIntersection(RayGPU& r, float& intersectionT, concurrency::array<Tri, 1>& models) const restrict(amp)
{
  return shape.TestIntersection(r, intersectionT, models);
}

void ObjectGPU::Update(void) restrict(cpu)
{
  switch (type)
  {
  case Player:
    {
    if(left)
    {
      if(shape.pos.x > -400.0f)
        shape.pos.x -= 10.0f;
    }

    if(right)
    {
      if(shape.pos.x < 400.0f)
        shape.pos.x += 10.0f;
    }

    if(fire)
    {
      ObjectGPU& obj = FindObjectType(Ball);
      if(obj.status != Ball_Flight)
      {
        obj.status = Ball_Flight;
        obj.direction.x = 0.0f;
        obj.direction.y = -1.0f;
        obj.direction.z = 0.0f;
      }
    }

    ObjectGPU& ball = FindObjectType(Ball);
    if((ball.shape.pos - shape.pos).Length() <= (ball.shape.radius + shape.radius))
    {
      ball.direction = ball.shape.pos - shape.pos;
      ball.direction.Normalize();
    }
  }
    break;

  case Ball:
    switch (status)
    {
    case Ball_Start:
      shape.pos = FindObjectType(Player).shape.pos + Point3GPU(0.0f, -50.0f -(1.5f * shape.radius), 0.0f);
      break;
    case Ball_Flight:
      shape.pos = shape.pos + (direction * 8.0f);

      if(shape.pos.y < (-450.0f + shape.radius))
        direction.y *= -1.0f;

      if(shape.pos.y > 500.0f)
      {        
        if(lives >= 0)
        {
          status = Ball_Start;

          auto i = g_GPUScene.begin();
          for(; i != g_GPUScene.end(); ++i)
          {
            if(i->type == Life)
            {
              g_GPUScene.erase(i);
              break;
            }
          }

          --lives;
        }
        else
        {
          ObjectGPU& player = FindObjectType(Player);
          player.material = *MaterialDatabase::GetMaterial("Lose");
        }
      }

      if(shape.pos.x > (400.0f - shape.radius))
        direction.x *= -1.0f;

      if(shape.pos.x < (-400.0f + shape.radius))
        direction.x *= -1.0f;

      TestBallCollision(*this);
      break;
    }

    if(TestWin())
    {
      ObjectGPU& player = FindObjectType(Player);
      player.material = *MaterialDatabase::GetMaterial("Win");
      lives = 5000;
    }
    break;

  case Brick:
    if(status == Brick_Collided)
    {
      prevRadius = shape.radius;// = 10.0f;
      ObjectGPU& ball = FindObjectType(Ball);
      ball.direction = ball.shape.pos - shape.pos;
      ball.direction.Normalize();
      status = Brick_Shrink;
    }
    else if(status == Brick_Shrink)
    {
      shape.radius = (0.8f * prevRadius) + (0.2f * 5.0f);
      prevRadius = shape.radius;

      if(shape.radius < 6.0f)
        status = Brick_Dead;
    }

    break;

  default:
    // Do nothing...
    //
    break;
  }
}
