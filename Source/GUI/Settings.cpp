#include "Settings.h"

SettingsDialog::SettingsDialog(const wxString& title) : wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(400,500))
{
  // Panel that will contain the majority of stuff
  //
  wxPanel* panel = new wxPanel(this);

  // Box sizers for organization
  //
  wxBoxSizer* vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer* hbox = new wxBoxSizer(wxHORIZONTAL);

  // Static box to contain things that are similar
  //
  wxStaticBox* cameraBox = new wxStaticBox(panel, -1, wxT("Camera"), wxPoint(5,5), wxSize(385, 110));

  // Controling camera window height
  //
  wxTextCtrl* heightBox = new wxTextCtrl(panel, ID_Settings_HeightBox, wxT("Height"), wxPoint(60,25));
  wxStaticText* heightLabel = new wxStaticText(panel, -1, wxT("Height:"), wxPoint(15,25));

  // Controling camera window width
  //
  wxTextCtrl* widthBox = new wxTextCtrl(panel, ID_Settings_WidthBox, wxT("Width"), wxPoint(60,50));
  wxStaticText* widthLabel = new wxStaticText(panel, -1, wxT("Width:"), wxPoint(15,50));

  // Controling image pos
  //
  wxTextCtrl* imgPosXBox = new wxTextCtrl(panel, ID_Settings_ImageX, wxT("X"), wxPoint(100,75), wxSize(35,20));
  wxTextCtrl* imgPosYBox = new wxTextCtrl(panel, ID_Settings_ImageY, wxT("Y"), wxPoint(140, 75), wxSize(35,20));
  wxTextCtrl* imgPosZBox = new wxTextCtrl(panel, ID_Settings_ImageZ, wxT("Z"), wxPoint(180, 75), wxSize(35,20));
  wxStaticText* imagePosLabel = new wxStaticText(panel, -1, wxT("Image Position:"), wxPoint(15,75));

  // Buttons to control the window
  //
  wxButton* ok = new wxButton(this, ID_Settings_Apply, wxT("Apply"), wxDefaultPosition, wxSize(70, 30));
  wxButton* close = new wxButton(this, ID_Settings_Close, wxT("Close"), wxDefaultPosition, wxSize(70, 30));

  // Add the buttons to the horiz box so they show up next to each other
  //
  hbox->Add(ok, 1);
  hbox->Add(close, 1, wxLEFT, 5);

  // Add the panel and buttons to the vert box so they show up on top of each other
  //
  vbox->Add(panel,1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  // Apply the vbox to the window
  //
  SetSizer(vbox);

  // Connect all functionality
  //
  Connect(ID_Settings_Apply, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&SettingsDialog::OnApply);
  Connect(ID_Settings_Close, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&SettingsDialog::OnClose);
  // TODO Connect text boxes
  //

  // Center in the screen and display the dialog
  //
  Centre();
  ShowModal();

  // Remove the dialog from memory
  //
  Destroy();
}

void SettingsDialog::OnApply(wxCommandEvent& event)
{

}

void SettingsDialog::OnClose(wxCommandEvent& event)
{
  // Close the settings dialog
  //
  Close(true);
}