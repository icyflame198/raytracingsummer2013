#pragma once

#include <wx\wx.h>

enum 
{
  ID_Settings_HeightBox = 1,
  ID_Settings_WidthBox,
  ID_Settings_ImageX,
  ID_Settings_ImageY,
  ID_Settings_ImageZ,
  ID_Settings_Apply,
  ID_Settings_Close
};

class SettingsDialog : public wxDialog
{
public:
  SettingsDialog(const wxString& title);

  void OnApply(wxCommandEvent& event);
  void OnClose(wxCommandEvent& event);
};