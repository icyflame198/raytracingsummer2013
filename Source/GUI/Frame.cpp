#include "Frame.h"
#include "Settings.h"

#include "../Camera/Camera.h"
#include "wx/progdlg.h" // Progress dialog

MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size) : wxFrame(NULL, -1, title, pos, size)
{
  // This is what the final GUI should look like
  // |------------------------------------------|
  // |---------------------------------| Render |
  // |                                 |Settings|
  // |        Ray Tracing Area         |        |
  // |                                 |        |
  // |_________________________________|________|
  //
  wxMenuBar* menuBar = new wxMenuBar;
  wxMenu* menuFile = new wxMenu;
  wxMenu* menuHelp = new wxMenu;

  // The main panel the image render and buttons will rest in
  //
  wxPanel* p = new wxPanel(this, wxID_ANY);

  // The sizer used to contain everything
  //
  wxBoxSizer* box = new wxBoxSizer(wxHORIZONTAL);

  // 2 panels one for rendering the results of the Ray Tracer, one for the buttons
  //
  imagePanel = new wxPanel(p, ID_Panel_Image, wxPoint(10,10), wxSize(700,590), wxBORDER_SIMPLE);
  wxPanel* buttonPanel = new wxPanel(p, wxID_NEW, wxPoint(701, 10), wxSize(150, 590), wxBORDER_SIMPLE);
  box->Add(imagePanel, 1, wxEXPAND | wxALL, 10);
  box->Add(buttonPanel, 0, wxEXPAND | wxALIGN_RIGHT | wxALL, 10);
  p->SetSizer(box);

  // This will make the menu look like 
  // File  Help
  //    ->Save
  //    ->-----------
  //    ->Exit
  menuFile->Append(ID_Menu_Save, _("&Save..."));
  menuFile->AppendSeparator();
  menuFile->Append(ID_Menu_Quit, _("E&xit"));

  menuHelp->Append(ID_Menu_About, _("&About..."));

  menuBar->Append(menuFile, _("&File"));
  menuBar->Append(menuHelp, _("&Help"));

  // Set the menu bar at the top of the window
  //
  SetMenuBar(menuBar);

  // Create lower bar on window
  //
  CreateStatusBar();

  // Sizer for the buttons which will be contained in the button panel
  //
  wxBoxSizer* buttonSizer = new wxBoxSizer(wxVERTICAL);

  // 3 Buttons one for rendering, one for the settings menu, one for refreshing the image
  //
  wxButton* render = new wxButton(buttonPanel, ID_Button_Render, wxT("Render"), wxPoint(5,5), wxSize(140, 35));
  wxButton* refresh = new wxButton(buttonPanel, ID_Button_Refresh, wxT("Refresh Image"), wxPoint(5, 50), wxSize(140, 35));
  wxButton* reload = new wxButton(buttonPanel, ID_Button_Reload, wxT("Reload Files"), wxPoint(5, 95), wxSize(140, 35));
  //wxButton* settings = new wxButton(buttonPanel, ID_Button_Settings, wxT("Settings..."),wxPoint(5, 140), wxSize(140, 35));
  buttonSizer->Add(render, 0, wxALL | wxALIGN_CENTER_HORIZONTAL | wxEXPAND, 5);
  buttonSizer->Add(refresh, 0, wxALL | wxALIGN_CENTER_HORIZONTAL | wxEXPAND, 5);
  buttonSizer->Add(reload, 0, wxALL | wxALIGN_CENTER_HORIZONTAL | wxEXPAND, 5);
  //buttonSizer->Add(settings, 0, wxALL | wxALIGN_CENTER_HORIZONTAL | wxEXPAND, 5);
  buttonPanel->SetSizer(buttonSizer);

  // Attach handler functions
  //
  Connect(ID_Menu_Quit, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction)&MyFrame::OnQuit);
  Connect(ID_Menu_About, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction)&MyFrame::OnAbout);
  Connect(ID_Menu_Save, wxEVT_COMMAND_MENU_SELECTED, (wxObjectEventFunction)&MyFrame::OnSave);
  Connect(ID_Button_Render, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MyFrame::OnRenderButtonClick);
  //Connect(ID_Button_Settings, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MyFrame::OnSettingsButtonClick);
  Connect(ID_Button_Refresh, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MyFrame::OnRefreshButtonClick);
  Connect(ID_Button_Reload, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MyFrame::OnReload);
  imagePanel->Connect(wxEVT_PAINT, (wxObjectEventFunction)&MyFrame::OnImagePaint, 0, this);

  // Initaliaze wxWidgets to be able to save files as png
  //
  wxInitAllImageHandlers();
}

void MyFrame::OnAbout(wxCommandEvent& event)
{
  // Display the about box
  //
  wxMessageBox( _("Author: David James\nVersion: 0.1\nDate: 05/13/13"), _("About"), wxOK|wxICON_INFORMATION, this );
}

void MyFrame::OnSave(wxCommandEvent& event)
{
  // Bring up the dialog of where to save the frame
  //
  wxFileDialog  dlg( this, _T("Save As..."), wxEmptyString, wxEmptyString, _T("*.png"), wxFD_SAVE );

  if(dlg.ShowModal() == wxID_OK)
  {
    // Get the path to the save location
    //
    wxString path = dlg.GetPath();

    // Convert from the raw data to something wxWidgets can use
    //
    Camera& cam = rt.GetCamera();
    wxImage image(cam.width, cam.height, cam.GetImagePlane(), true);

    // Use wxWidgets to save the file as a PNG
    //
    image.SaveFile(path, wxBITMAP_TYPE_PNG);
  }
}

void MyFrame::OnQuit(wxCommandEvent& event)
{
  // Exit the program
  //
  Close(true);
}

void MyFrame::OnRenderButtonClick(wxCommandEvent& event)
{
  wxProgressDialog dlg (wxT("Progress..."), wxT("Rendering Image"), rt.GetCamera().width * rt.GetCamera().height);
  dlg.SetSize(300, 150);
  
  DWORD startTime = timeGetTime();
  //rt.Render(&dlg);
  rt.RenderRecursive(&dlg);

  // Detrmine how long it took to render
  //
  startTime = timeGetTime() - startTime;
  char buffer[32] = {0};
  #pragma warning (disable:4996)
  sprintf(buffer, "Render Time: %.3f s", static_cast<float>(startTime / 1000.0f));
  #pragma warning (default:4996)
  SetStatusText(buffer);

  // Refresh the image
  //
  Refresh();
}

void MyFrame::OnSettingsButtonClick(wxCommandEvent& event)
{
  SettingsDialog sd(wxT("Settings"));
  sd.Show(true);
}

void MyFrame::OnRefreshButtonClick(wxCommandEvent& event)
{
  // Simply Refresh the window
  //
  Refresh();
}

void MyFrame::OnImagePaint(wxPaintEvent& event)
{
  wxPaintDC dc(imagePanel);
  PrepareDC(dc);

  // Get the uchar data from the ray tracer and create a wxImage from it
  //
  Camera& cam = rt.GetCamera();
  wxImage image(cam.width, cam.height, cam.GetImagePlane(), true);

  // Rescale that image to the ray tracing area size
  //
  image.Rescale(imagePanel->GetSize().x, imagePanel->GetSize().y);
  
  // Display the frame with a call to dc.DrawBitmap
  //
  dc.DrawBitmap(image, wxPoint(0,0));
}

void MyFrame::OnReload(wxCommandEvent& event)
{
  // Reload the settings
  //
  rt.Reload();
}

