#include "App.h"
#include "Frame.h"

// This is where main / winmain is located
//
IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
  // _() is a way to convert strings to wxStrings
  //
  MyFrame* frame = new MyFrame(_("Ray Tracer"), wxPoint(50,50), wxSize(800, 600));

  frame->Show(true);
  SetTopWindow(frame);
  return true;
}