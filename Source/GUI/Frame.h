#pragma once

#include <wx\wx.h>
#include "../RayTracer/RayTracer.h"

// Unique ID for menu items
//
enum
{
  ID_Menu_Quit = 1,
  ID_Menu_About,
  ID_Menu_Save,
  ID_Button_Render,
  ID_Button_Settings,
  ID_Button_Refresh,
  ID_Button_Reload,
  ID_Panel_Image
};

// The main GUI class
//
class MyFrame : public wxFrame
{
  public:
    MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size);

    void OnQuit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnSave(wxCommandEvent& event);
    void OnRenderButtonClick(wxCommandEvent& event);
    void OnSettingsButtonClick(wxCommandEvent& event);
    void OnImagePaint(wxPaintEvent& event);
    void OnSize(wxSizeEvent& event);
    void OnRefreshButtonClick(wxCommandEvent& event);
    void OnReload(wxCommandEvent& event);

private:
  wxPanel* imagePanel;
  RayTracer rt;
};