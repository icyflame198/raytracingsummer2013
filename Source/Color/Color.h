#pragma once

struct Color
{
  Color(void);
  Color(float r, float g, float b);
  Color(const Color& c);
  Color& operator=(const Color& c);

  void SetColor(float r, float g, float b);

  Color operator+(const Color& rhs);
  Color operator*(const float rhs);

  float red;
  float green;
  float blue;
};