#include "Color.h"
#include <algorithm>

Color::Color(void) : red(0), green(0), blue(0)
{
}

Color::Color(float r, float g, float b) : red(std::min(r, 1.0f)),
                                          green(std::min(g, 1.0f)), 
                                          blue(std::min(b, 1.0f))
{
}

Color::Color(const Color& c) : red(c.red), green(c.green), blue(c.blue)
{
}

Color& Color::operator=(const Color& c)
{
  if(this == &c)
    return * this;

  red = c.red;
  green = c.green;
  blue = c.blue;

  return *this;
}

Color Color::operator+(const Color& rhs)
{
  return Color(red + rhs.red, green + rhs.green, blue + rhs.blue);
}

Color Color::operator*(const float rhs)
{
  return Color(red * rhs, green * rhs, blue * rhs);
}

void Color::SetColor(float r, float g, float b)
{
  // Using floats for more precision
  //
  red = std::min(r, 1.0f);
  green = std::min(g, 1.0f);
  blue = std::min(b, 1.0f);
}