#pragma once

#include "../Color/Color.h"

struct Material
{
  Material(Color& c, float r, bool texture);
  ~Material(void);

  Color color;
  float reflectivity;
  bool isTex;
  Color* texture;

  Color GetTexColor(float u, float v);
  void LoadTexture(const char* filename);
};