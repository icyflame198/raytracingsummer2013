#pragma once

struct Point3;

struct Vec3
{
  Vec3(void);
  Vec3(float x_, float y_, float z_, float w_ = 0.0f);
  Vec3(const Vec3& v);

  // Data
  //
  float x, y, z, w;

  // Methods
  //
  float Dot(Vec3& v);
  float Dot(Point3& p);
  float Length(void);
  float SquaredLength(void);
  void Normalize(void);
  Vec3 CrossProduct(Vec3& v);

  // Assignment operator
  //
  Vec3& operator=(const Vec3& rhs);

  // Multiplication, Division, Addition, and subtraction operators
  //
  Vec3 operator*(const float rhs);
  Vec3 operator*(const Vec3& rhs);

  Vec3 operator/(const float rhs);
  Vec3 operator/(const Vec3& rhs);

  Vec3 operator+(const float rhs);
  Vec3 operator+(const Vec3& rhs);
  Point3 operator+(const Point3& rhs);

  Vec3 operator-(const float rhs);
  Vec3 operator-(const Vec3& rhs);

  Vec3& operator+=(const float rhs);
  Vec3& operator+=(const Vec3& rhs);

  Vec3& operator-=(const float rhs);
  Vec3& operator-=(const Vec3& rhs);

  Vec3& operator*=(const float rhs);
  Vec3& operator*=(const Vec3& rhs);

  Vec3& operator/=(const float rhs);
  Vec3& operator/=(const Vec3& rhs);
};