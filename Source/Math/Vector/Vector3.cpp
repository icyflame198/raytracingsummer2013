#include "Vector3.h"
#include "../Point/Point.h"
#include <math.h> // sqrt

// Ctors, assignment operator
//
Vec3::Vec3(void) : x(0), y(0), z(0), w(0.0f)
{

}

Vec3::Vec3(float x_, float y_, float z_, float w_) : x(x_), y(y_), z(z_), w(w_)
{

}

Vec3::Vec3(const Vec3& rhs) : x(rhs.x), y(rhs.y), z(rhs.z)
{

}

Vec3& Vec3::operator=(const Vec3& rhs)
{
  // No need to worry about self assignment here since there is no allocated data
  //
  x = rhs.x;
  y = rhs.y;
  z = rhs.z;

  return *this;
}

// Methods
//
float Vec3::Dot(Vec3& v)
{
  return x * v.x + y * v.y + z * v.z;
}

float Vec3::Dot(Point3& p)
{
  return x * p.x + y * p.y + z * p.z;
}

float Vec3::Length(void)
{
  return sqrt(SquaredLength());
}

float Vec3::SquaredLength(void)
{
  return x * x + y * y + z * z;
}

void Vec3::Normalize(void)
{
  *this /= Length();
}

Vec3 Vec3::CrossProduct(Vec3& v)
{
  Vec3 crossedVec;

  crossedVec.x = y * v.z - z * v.y;
  crossedVec.y = x * v.z - z * v.x;
  crossedVec.z = x * v.y - y * v.x;

  return crossedVec;
}

// Operators
//
Vec3& Vec3::operator*=(const float rhs)
{
  x *= rhs;
  y *= rhs;
  z *= rhs;

  return *this;
}

Vec3& Vec3::operator*=(const Vec3& rhs)
{
  x *= rhs.x;
  y *= rhs.y;
  z *= rhs.z;

  return *this;
}

Vec3& Vec3::operator/=(const float rhs)
{
  x /= rhs;
  y /= rhs;
  z /= rhs;

  return *this;
}

Vec3& Vec3::operator/=(const Vec3& rhs)
{
  x /= rhs.x;
  y /= rhs.y;
  z /= rhs.z;

  return *this;
}

Vec3& Vec3::operator+=(const float rhs)
{
  x += rhs;
  y += rhs;
  z += rhs;

  return *this;
}

Vec3& Vec3::operator+=(const Vec3& rhs)
{
  x += rhs.x;
  y += rhs.y;
  z += rhs.z;

  return *this;
}

Vec3& Vec3::operator-=(const float rhs)
{
  x -= rhs;
  y -= rhs;
  z -= rhs;

  return *this;
}

Vec3& Vec3::operator-=(const Vec3& rhs)
{
  x -= rhs.x;
  y -= rhs.y;
  z -= rhs.z;

  return *this;
}

Vec3 Vec3::operator*(const float rhs)
{
  Vec3 v(*this);

  v *= rhs;

  return v;
}

Vec3 Vec3::operator*(const Vec3& rhs)
{
  Vec3 v(*this);

  v *= rhs;

  return v;
}

Vec3 Vec3::operator/(const float rhs)
{
  Vec3 v(*this);

  v /= rhs;

  return v;
}

Vec3 Vec3::operator/(const Vec3& rhs)
{
  Vec3 v(*this);

  v /= rhs;

  return v;
}

Vec3 Vec3::operator+(const float rhs)
{
  Vec3 v(*this);

  v += rhs;

  return v;
}

Vec3 Vec3::operator+(const Vec3& rhs)
{
  Vec3 v(*this);

  v += rhs;

  return v;
}

Point3 Vec3::operator+(const Point3& rhs)
{
  return Point3(x + rhs.x, y + rhs.y, z + rhs.z);
}

Vec3 Vec3::operator-(const float rhs)
{
  Vec3 v(*this);

  v -= rhs;

  return v;
}

Vec3 Vec3::operator-(const Vec3& rhs)
{
  Vec3 v(*this);

  v -= rhs;

  return v;
}







