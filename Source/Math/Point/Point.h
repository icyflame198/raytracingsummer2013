#pragma once

struct Vec3;

struct Point3
{
  Point3(void);
  Point3(float x_, float y_, float z_, float w_ = 1.0f);
  Point3(const Point3& v);

  // Data
  //
  float x, y, z, w;
  float u, v;

  // Methods
  //
  float Distance(Point3& p);
  float DistanceSquared(Point3& p);

  // Assignment operator
  //
  Point3& operator=(const Point3& rhs);

  // Needed operators
  //
  Vec3 operator-(const Point3& rhs);
  Point3 operator+(const Vec3& rhs);
  Point3 operator+(const Point3& rhs);
};