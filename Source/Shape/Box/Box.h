#pragma once

#include "../Parent/Shape.h"
#include "../../Math/Point/Point.h"

class Box : public Shape
{
public:
  Box(void);
  Box(const Box& rhs);
  ~Box(void);
  Box& operator=(const Box& rhs);

  virtual bool TestIntersection(Ray& r, float* inptersectionT);
  virtual void Import(tinyxml2::XMLElement* root);
  virtual Vec3 FindNormal(Point3& p);

private:
  Point3 center;
  Vec3 dimensions;
};