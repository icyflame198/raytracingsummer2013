#include "Sphere.h"

#include <cmath>

Sphere::Sphere(void) : Shape("Sphere"), origin(), radius(1)
{
}

Sphere::Sphere(const Sphere& rhs) : Shape(rhs.typeName), origin(rhs.origin), radius(rhs.radius)
{
}

Sphere::~Sphere(void)
{
}

Sphere& Sphere::operator=(const Sphere& rhs)
{
  if(this == &rhs)
    return *this;

  origin = rhs.origin;
  radius = rhs.radius;

  return *this;
}

bool Sphere::TestIntersection(Ray& r, float* t)
{
  Vec3 m = r.origin - origin;
  float b = m.Dot(r.direction); 
  float c = m.Dot(m) - (radius * radius);

  if(c > 0.0f && b > 0.0f)
    return false;

  float discr = (b * b) - c;

  if(discr < 0.0f)
    return false;

  *t = -b - sqrt(discr);
  if(*t < 0.0f)
    *t = 0.0f;

  return true;
}

void Sphere::Import(tinyxml2::XMLElement* element)
{
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Pos") == 0)
    {
      it->QueryFloatAttribute("x", &origin.x);
      it->QueryFloatAttribute("y", &origin.y);
      it->QueryFloatAttribute("z", &origin.z);
    }
    else if(strcmp(it->Value(), "Radius") == 0)
    {
      it->QueryFloatAttribute("r", &radius);
    }
  }
}

Vec3 Sphere::FindNormal(Point3& p)
{
  Vec3 n = p - origin;
  n.Normalize();

  Vec3 d = origin - p;
  d.Normalize();
  u = 0.5f + (std::atan2(d.z, d.x) / 6.28318f);
  v = 0.5f - 2.0f * (std::asin(d.y) / 6.28318f);

  return n;
}

void Sphere::GetTexCoords(float* U, float* V)
{
  *U = u;
  *V = v;
}