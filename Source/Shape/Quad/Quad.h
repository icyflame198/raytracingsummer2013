#pragma once

#include "../Parent/Shape.h"
#include "../Triangle/Triangle.h"

class Quad : public Shape
{
public:
  Quad(void);
  Quad(const Quad& rhs);
  ~Quad(void);
  Quad& operator=(const Quad& rhs);

  virtual bool TestIntersection(Ray& r, float* inptersectionT);
  virtual void Import(tinyxml2::XMLElement* root);
  virtual Vec3 FindNormal(Point3& p);
  virtual void GetTexCoords(float* u, float* v);

  Triangle t0;
  Triangle t1;

  Point3 pos;
  Point3 scale;
  Point3 rotation;

  bool t0Inter;
  bool t1Inter;
private:
};