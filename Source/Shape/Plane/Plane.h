#pragma once

#include "../Parent/Shape.h"

class Plane : public Shape
{
public:
  Plane(void);
  Plane(const Plane& rhs);
  ~Plane(void);
  Plane& operator=(const Plane& rhs);

  virtual bool TestIntersection(Ray& r, float* t);
  virtual void Import(tinyxml2::XMLElement* root);
  virtual Vec3 FindNormal(Point3& p);
  virtual void GetTexCoords(float* U, float* V);

  Vec3 normal;
  Point3 pos;
  float d;

private:
};