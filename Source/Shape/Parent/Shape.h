#pragma once

#include "../../Math/Ray/Ray.h"
#include "../../Math/Point/Point.h"
#include "../../Math/Vector/Vector3.h"
#include "../../TinyXML/tinyxml2.h"

#include <math.h>

class Shape
{
public:
  Shape(const char* name);
  virtual ~Shape(void);
  virtual bool TestIntersection(Ray& r, float* intersetionT) = 0;
  virtual void Import(tinyxml2::XMLElement* root) = 0;
  virtual Vec3 FindNormal(Point3& p) = 0;
  virtual void GetTexCoords(float* u, float* v) = 0;
  const char* typeName;
private:

};