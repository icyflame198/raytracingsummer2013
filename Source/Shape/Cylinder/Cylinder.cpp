#include "Cylinder.h"

Cylinder::Cylinder(void) : Shape("Cylinder")
{
}

Cylinder::Cylinder(const Cylinder& rhs) : Shape("Cylinder")
{
}

Cylinder& Cylinder::operator=(const Cylinder& rhs)
{
  if(this == &rhs)
    return *this;

  

  return *this;
}

Cylinder::~Cylinder(void)
{
}

bool Cylinder::TestIntersection(Ray& r, float* t)
{
  return false;
}

void Cylinder::Import(tinyxml2::XMLElement* element)
{
  for(auto it = element->FirstChildElement(); it != NULL; it = it->NextSiblingElement())
  {
    if(strcmp(it->Value(), "Pos") == 0)
    {
      
    }
    else if(strcmp(it->Value(), "Normal") == 0)
    {
      
    }
  }
}

Vec3 Cylinder::FindNormal(Point3& p)
{
  return Vec3();
}