#include "MaterialDB.h"

#include "../Color/Color.h"
#include "../Material/Material.h"

#include "../TinyXML/tinyxml2.h"

stdext::hash_map<std::string, Material*> MaterialDatabase::materials;

void MaterialDatabase::CleanDatabase(void)
{
  for(auto it = materials.begin(); it != materials.end(); ++it)
  {
    delete it->second;
  }

  materials.clear();
}

void MaterialDatabase::AddMaterial(std::string name, Material* mat)
{
  if(materials.find(name) == materials.end())
  {
    materials[name] = mat;
  }
  else
  {
    delete mat;
  }
}

Material* MaterialDatabase::GetMaterial(std::string name)
{
  return materials[name];
}

void MaterialDatabase::LoadFromFile(const char* filename)
{
  tinyxml2::XMLDocument doc;
  
  // If file loaded successfully
  //
  if(doc.LoadFile(filename) == tinyxml2::XML_NO_ERROR)
  {
    for(tinyxml2::XMLElement* element = doc.FirstChildElement(); element != NULL; element = element->NextSiblingElement())
    {
      const char* name = element->Attribute("name");
      int r,g,b;
      float reflectivity;
      bool isTex = false;

      element->QueryIntAttribute("red", &r);
      element->QueryIntAttribute("green", &g);
      element->QueryIntAttribute("blue", &b);

      element->QueryFloatAttribute("reflectivity", &reflectivity);

      element->QueryBoolAttribute("isTex", &isTex);

      AddMaterial(name, new Material(Color(static_cast<float>(r) / 255.0f, static_cast<float>(g) / 255.0f, static_cast<float>(b) / 255.0f), reflectivity, isTex));

      if(isTex)
        materials[name]->LoadTexture("");
    }
  }
  else
  {
    // TODO proper error handling
    //
  }
}