#include "Factory.h"

stdext::hash_map<std::string, Prototype*> ShapeFactory::shapes;

void ShapeFactory::CleanFactory(void)
{
  for(auto it = shapes.begin(); it != shapes.end(); ++it)
  {
    delete it->second;
  }

  shapes.clear();
}

void ShapeFactory::RegisterPrototype(const char* name, Prototype* type)
{
  if(shapes.find(name) == shapes.end())
  {
    shapes[name] = type;
  }
  else
  {
    delete type;
  }
  
}