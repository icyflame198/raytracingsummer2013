#pragma once

#include <hash_map>
#include <string>


class Prototype
{
public:
  virtual void* CreateType(void) = 0;
};

template <typename T>
class TypedPrototype : public Prototype
{
public:
  void* CreateType(void)
  {
    return new T();
  }

  ~TypedPrototype(void)
  {
    delete T;
  }
};

struct ShapeFactory
{
  static void RegisterPrototype(const char* name, Prototype* object);
  static stdext::hash_map<std::string, Prototype*> shapes;
  static void CleanFactory(void);
};