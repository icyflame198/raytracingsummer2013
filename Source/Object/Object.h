#pragma once 

#include "../Math/Ray/Ray.h"
#include "../Material/Material.h"

class Shape;

class Object
{
public:
  Object(void);
  Object(const Object& rhs);
  Object& operator=(const Object& rhs);
  ~Object(void);

  bool TestIntersection(Ray& r, float* intersectionT);

  Shape* shape;
  Material* material;
private:
  

};