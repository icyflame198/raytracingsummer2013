#include "Object.h"

#include "../Shape/Parent/Shape.h"
#include "../Factory/Factory.h"

Object::Object(void) : shape(NULL)
{
}

Object::Object(const Object& rhs)
{
  // TODO Make a function that takes a name and returns an object
  //
  shape = reinterpret_cast<Shape*>(ShapeFactory::shapes[rhs.shape->typeName]->CreateType());
}

Object& Object::operator=(const Object& rhs)
{
  // TODO Make a function that takes a name and returns an object
  //
  shape = reinterpret_cast<Shape*>(ShapeFactory::shapes[rhs.shape->typeName]->CreateType());

  return *this;
}

Object::~Object(void)
{
  if(shape)
  {
    delete shape;
  }
}

bool Object::TestIntersection(Ray& r, float* intersectionT)
{
  return shape->TestIntersection(r, intersectionT);;
}