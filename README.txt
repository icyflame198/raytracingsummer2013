Author: David James
Email: david.james@digipen.edu
About: A simple implementation of the ray Tracing algorithm ported to the GPU to run in real-time
       There are 2 versions implemented here, one is on the CPU which will render one frame that can
       be saved, the other is a real time implementation on the GPU that has a small block breaker
       game. 

VS2012 folder uses Visual Studio 2012 and contains the solution for the real time ray tracer
VS2010 folder uses Visual Studio 2010 and features a CPU based ray tracer
Source contains source code
Resources contains the xml files 
A settings.xml file is found in both 2010 and 2012 this contains where the ray tracer shoud look for the resources
DirectX Ray Tracer contains the project file for the DirectX real time ray tracer, this one will use the gpuscene.xml

Using TinyXML2:
Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

   1. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

   1. This notice may not be removed or altered from any source distribution.

Game Controls:
A - Move Left
D - Move Right
Space - Fire ball
Esc - Exit

Goal:
Destroy all of the green and blue spheres

Win/Lose:
Win - Your ball will turn green
Lose - Your ball will turn red

Notes:
The collision on the side of the wall is invisible
The more objects you destroy the higher your frame rate will be which in turn will make everything move faster
The gold balls on the left are your remaining lives
The frame rate is displayed as the window title

